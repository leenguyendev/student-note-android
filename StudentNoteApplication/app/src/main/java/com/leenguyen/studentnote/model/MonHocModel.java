package com.leenguyen.studentnote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MonHocModel implements Parcelable {
    private String he_so_mon, muc_tieu, ten_mon_hoc, is_mon_chuyen;
    private DiemMonHocModel hoc_ky_1, hoc_ky_2;

    public MonHocModel(String ten_mon_hoc, String muc_tieu, String he_so_mon,  String is_mon_chuyen, DiemMonHocModel hoc_ky_1, DiemMonHocModel hoc_ky_2) {
        this.he_so_mon = he_so_mon;
        this.muc_tieu = muc_tieu;
        this.ten_mon_hoc = ten_mon_hoc;
        this.hoc_ky_1 = hoc_ky_1;
        this.hoc_ky_2 = hoc_ky_2;
        this.is_mon_chuyen = is_mon_chuyen;
    }

    public MonHocModel() {
    }

    protected MonHocModel(Parcel in) {
        he_so_mon = in.readString();
        muc_tieu = in.readString();
        ten_mon_hoc = in.readString();
        is_mon_chuyen = in.readString();
    }

    public static final Creator<MonHocModel> CREATOR = new Creator<MonHocModel>() {
        @Override
        public MonHocModel createFromParcel(Parcel in) {
            return new MonHocModel(in);
        }

        @Override
        public MonHocModel[] newArray(int size) {
            return new MonHocModel[size];
        }
    };

    public String getIs_mon_chuyen() {
        return is_mon_chuyen;
    }

    public void setIs_mon_chuyen(String is_mon_chuyen) {
        this.is_mon_chuyen = is_mon_chuyen;
    }

    public String getHe_so_mon() {
        return he_so_mon;
    }

    public void setHe_so_mon(String he_so_mon) {
        this.he_so_mon = he_so_mon;
    }

    public String getMuc_tieu() {
        return muc_tieu;
    }

    public void setMuc_tieu(String muc_tieu) {
        this.muc_tieu = muc_tieu;
    }

    public String getTen_mon_hoc() {
        return ten_mon_hoc;
    }

    public void setTen_mon_hoc(String ten_mon_hoc) {
        this.ten_mon_hoc = ten_mon_hoc;
    }

    public DiemMonHocModel getHoc_ky_1() {
        return hoc_ky_1;
    }

    public void setHoc_ky_1(DiemMonHocModel hoc_ky_1) {
        this.hoc_ky_1 = hoc_ky_1;
    }

    public DiemMonHocModel getHoc_ky_2() {
        return hoc_ky_2;
    }

    public void setHoc_ky_2(DiemMonHocModel hoc_ky_2) {
        this.hoc_ky_2 = hoc_ky_2;
    }

    @Override
    public String toString() {
        return "MonHocModel{" +
                "he_so_mon='" + he_so_mon + '\'' +
                ", muc_tieu='" + muc_tieu + '\'' +
                ", ten_mon_hoc='" + ten_mon_hoc + '\'' +
                ", is_mon_chuyen='" + is_mon_chuyen + '\'' +
                ", hoc_ky_1=" + hoc_ky_1 +
                ", hoc_ky_2=" + hoc_ky_2 +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(he_so_mon);
        dest.writeString(muc_tieu);
        dest.writeString(ten_mon_hoc);
        dest.writeString(is_mon_chuyen);
    }
}
