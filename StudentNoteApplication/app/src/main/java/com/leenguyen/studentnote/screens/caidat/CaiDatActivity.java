package com.leenguyen.studentnote.screens.caidat;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.databinding.ActivityCaiDatBinding;
import com.leenguyen.studentnote.screens.caidat.fragment.AnLopHocFragment;
import com.leenguyen.studentnote.screens.caidat.fragment.DoiPasswordFragment;
import com.leenguyen.studentnote.screens.caidat.fragment.DoiTenFragment;
import com.leenguyen.studentnote.screens.caidat.fragment.MatKhauMoUngDungFragment;
import com.leenguyen.studentnote.screens.caidat.fragment.MenuCaiDatFragment;

public class CaiDatActivity extends BaseActivity<ActivityCaiDatBinding> {
    public static final int MENU_POS = 0;
    public static final int DOI_TEN_POS = 1;
    public static final int DOI_PASS_POS = 2;
    public static final int AN_LOP_POS = 3;
    public static final int DOI_MK_MO_APP_POS = 4;
    private Fragment[] fragments;
    private int currentPos = 0;
    private SaveClickListener listener;

    @Override
    protected void initLayout() {
        fragments = new Fragment[]{new MenuCaiDatFragment(), new DoiTenFragment(), new DoiPasswordFragment(), new AnLopHocFragment(), new MatKhauMoUngDungFragment()};
        openFragment(MENU_POS);
        binding.btnBack.setOnClickListener(v -> {
            if (currentPos == MENU_POS) {
                finish();
            } else {
                openFragment(MENU_POS);
            }
        });

        binding.btnSave.setOnClickListener(v -> listener.saveClickListener());
    }

    public void openFragment(int pos) {
        binding.btnSave.setVisibility(pos == AN_LOP_POS ? View.VISIBLE : View.GONE);
        currentPos = pos;
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragments[pos], "openFragment")
                .addToBackStack(null)
                .commit();
    }

    public void setTitle(String title) {
        binding.txtTitle.setText(title);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cai_dat;
    }

    @Override
    public void onBackPressed() {
        if (currentPos == MENU_POS) {
            finish();
        } else {
            openFragment(MENU_POS);
        }
    }

    public void setSaveClickListener(SaveClickListener listener) {
        this.listener = listener;
    }
}