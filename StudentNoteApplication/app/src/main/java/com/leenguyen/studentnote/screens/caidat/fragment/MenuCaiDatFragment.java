package com.leenguyen.studentnote.screens.caidat.fragment;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.databinding.FragmentMenuCaiDatBinding;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.utils.Utils;

public class MenuCaiDatFragment extends BaseFragment<FragmentMenuCaiDatBinding> {
    private static final String LINK_FAN_PAGE = "https://www.facebook.com/HSVN-109333954940835";
    protected DatabaseReference versionDatabase;
    private String currentVersion;

    @SuppressLint("SetTextI18n")
    @Override
    protected void initLayout() {
        versionDatabase = FirebaseDatabase.getInstance().getReference();
        ((CaiDatActivity) getActivity()).setTitle(getString(R.string.cai_dat));
        currentVersion = Utils.getInstance().getCurrentVersion(getContext());
        binding.txtVersion.setText("Version " + currentVersion);
        binding.cardDoiTen.setOnClickListener(v -> ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.DOI_TEN_POS));
        binding.cardDoiPass.setOnClickListener(v -> ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.DOI_PASS_POS));
        binding.cardMaMoApp.setOnClickListener(v -> ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.DOI_MK_MO_APP_POS));
        binding.cardHideClass.setOnClickListener(v -> ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.AN_LOP_POS));
        binding.cardFacebook.setOnClickListener(v -> checkNetworkAndOpenChrome());
        binding.cardVersion.setOnClickListener(v -> checkVersion());
    }

    private void checkVersion() {
        versionDatabase.child("student_note_hoc_sinh").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String newVersion = snapshot.getValue(String.class);
                if (newVersion.equals(currentVersion)) {
                    Toast.makeText(getContext(), getString(R.string.ung_dung_da_cap_nhat_ban_moi_nhat), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), getString(R.string.da_co_phien_ban_moi) + " " + newVersion + " mới nhất", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void checkNetworkAndOpenChrome() {
        if (!Utils.getInstance().isOnline(getContext())) {
            Toast.makeText(getContext(), "Cần có kết nối mạng", Toast.LENGTH_LONG).show();
        } else {
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + MenuCaiDatFragment.LINK_FAN_PAGE);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                if (getActivity() != null) {
                    if (i.resolveActivity(getActivity().getPackageManager()) == null) {
                        i.setData(Uri.parse(MenuCaiDatFragment.LINK_FAN_PAGE));
                    }
                    getActivity().startActivity(i);
                }

            } catch (ActivityNotFoundException e) {
                // Chrome is not installed
                Toast.makeText(getContext(), getString(R.string.can_co_chrome), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_menu_cai_dat;
    }
}
