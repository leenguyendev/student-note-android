package com.leenguyen.studentnote.screens.sukien.fragment;

import android.widget.Toast;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentThemSuKienBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.screens.sukien.SaveClickListener;
import com.leenguyen.studentnote.screens.sukien.SuKienActivity;
import com.leenguyen.studentnote.screens.sukien.model.SuKienModel;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.Calendar;

public class ThemSuKienFragment extends BaseFragment<FragmentThemSuKienBinding> implements SaveClickListener {
    private int myDayOfMoth, myMonth, myYear;
    private String username;

    @Override
    protected void initLayout() {
        ((SuKienActivity) getActivity()).setTitle(getString(R.string.them_su_kien));
        ((SuKienActivity) getActivity()).setSaveClickListener(this);
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        Calendar calendar = Calendar.getInstance();
        myDayOfMoth = calendar.get(Calendar.DAY_OF_MONTH);
        myMonth = calendar.get(Calendar.MONTH);
        myYear = calendar.get(Calendar.YEAR);

        binding.calendar.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            myDayOfMoth = dayOfMonth;
            myMonth = month;
            myYear = year;
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_them_su_kien;
    }

    private void luuSuKienMoi() {
        String tenSuKien = binding.edtTenSuKien.getText().toString();
        String diaDiem = binding.edtDiaDiem.getText().toString();
        if (tenSuKien.trim().length() == 0) {
            Toast.makeText(getContext(), getString(R.string.ban_chua_nhap_ten_su_kien), Toast.LENGTH_SHORT).show();
            return;
        }
        String buoi;
        if (binding.rdbSang.isChecked()) {
            buoi = "Buổi sáng";
        } else if (binding.rdbChieu.isChecked()) {
            buoi = "Buổi chiều";
        } else {
            buoi = "Buổi tối";
        }
        String tiet = String.valueOf(binding.pkTiet.getValue());
        String ngay = myDayOfMoth < 10 ? "0" + myDayOfMoth : String.valueOf(myDayOfMoth);
        String month = myMonth < 10 ? "0" + (myMonth + 1) : String.valueOf(myMonth + 1);
        String year = String.valueOf(myYear);
        String thoiGian = year + month + ngay;
        SuKienModel suKienModel = new SuKienModel(thoiGian, tenSuKien, buoi, tiet, diaDiem);
        database.child(username).child(Data.SU_KIEN).child(Utils.getInstance().getCurrentTime()).setValue(suKienModel);
        binding.edtTenSuKien.setText("");
        binding.edtDiaDiem.setText("");
        Toast.makeText(getContext(), getString(R.string.them_su_kien_thanh_cong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveClickListener() {
        if (!Utils.getInstance().isOnline(getContext())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.can_co_ket_noi_mang), null);
            return;
        }
        luuSuKienMoi();
    }
}
