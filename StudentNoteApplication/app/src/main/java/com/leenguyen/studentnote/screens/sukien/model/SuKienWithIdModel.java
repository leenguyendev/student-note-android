package com.leenguyen.studentnote.screens.sukien.model;

public class SuKienWithIdModel {
    private String id;
    private SuKienModel suKienModel;

    public SuKienWithIdModel(String id, SuKienModel suKienModel) {
        this.id = id;
        this.suKienModel = suKienModel;
    }

    public SuKienWithIdModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SuKienModel getSuKienModel() {
        return suKienModel;
    }

    public void setSuKienModel(SuKienModel suKienModel) {
        this.suKienModel = suKienModel;
    }
}
