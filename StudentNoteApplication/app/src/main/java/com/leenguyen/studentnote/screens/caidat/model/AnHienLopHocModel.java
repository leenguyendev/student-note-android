package com.leenguyen.studentnote.screens.caidat.model;

public class AnHienLopHocModel {
    private String tenLopHoc;
    private boolean isDisplay;

    public AnHienLopHocModel(String tenLopHoc, boolean isDisplay) {
        this.tenLopHoc = tenLopHoc;
        this.isDisplay = isDisplay;
    }

    public AnHienLopHocModel() {
    }

    public String getTenLopHoc() {
        return tenLopHoc;
    }

    public void setTenLopHoc(String tenLopHoc) {
        this.tenLopHoc = tenLopHoc;
    }

    public boolean isDisplay() {
        return isDisplay;
    }

    public void setDisplay(boolean display) {
        isDisplay = display;
    }
}
