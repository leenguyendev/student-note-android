package com.leenguyen.studentnote.screens.themmonhoc.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.ItemThemNhanhMonHocBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThemDanhSachMonHocAdapter extends RecyclerView.Adapter<ThemDanhSachMonHocAdapter.ThemDanhSachMonHocHolder> {

    private Context context;
    private List<String> list;
    private String[] listAdded;

    public ThemDanhSachMonHocAdapter(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<String> list) {
        this.list = list;
        listAdded = new String[list.size()];
        notifyDataSetChanged();
    }

    public List<String> getListAdded() {
        return Arrays.asList(listAdded);
    }

    @NonNull
    @Override
    public ThemDanhSachMonHocHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemThemNhanhMonHocBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_them_nhanh_mon_hoc, parent, false);
        return new ThemDanhSachMonHocHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ThemDanhSachMonHocHolder holder, int position) {
        holder.binding.txtTenMonHoc.setText(list.get(position));
        holder.binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                listAdded[position] = list.get(position);
            } else {
                listAdded[position] = null;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class ThemDanhSachMonHocHolder extends RecyclerView.ViewHolder {
        private final ItemThemNhanhMonHocBinding binding;

        public ThemDanhSachMonHocHolder(ItemThemNhanhMonHocBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
