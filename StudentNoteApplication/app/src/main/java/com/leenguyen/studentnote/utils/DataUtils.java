package com.leenguyen.studentnote.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class DataUtils {

    private static DataUtils instance;

    public static DataUtils getInstance() {
        if (instance == null) {
            instance = new DataUtils();
        }
        return instance;
    }

    public void saveValue (Context context, String sharePreferences, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(sharePreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getValue (Context context, String sharePreferences, String key) {
        SharedPreferences preferences = context.getSharedPreferences(sharePreferences, Context.MODE_PRIVATE);
        return preferences.getString(key, "null");
    }
}
