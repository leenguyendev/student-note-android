package com.leenguyen.studentnote.screens.sukien.fragment;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentSuKienBinding;
import com.leenguyen.studentnote.screens.sukien.DeleteSuKienListener;
import com.leenguyen.studentnote.screens.sukien.SuKienActivity;
import com.leenguyen.studentnote.screens.sukien.adapter.SuKienAdapter;
import com.leenguyen.studentnote.screens.sukien.model.SuKienModel;
import com.leenguyen.studentnote.screens.sukien.model.SuKienWithIdModel;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;

public class SuKienFragmnet extends BaseFragment<FragmentSuKienBinding>  implements DeleteSuKienListener {
    private SuKienAdapter adapter;
    private ArrayList<SuKienWithIdModel> list;
    private String username;
    @Override
    protected void initLayout() {
        ((SuKienActivity)getActivity()).setTitle(getString(R.string.su_kien));
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        adapter = new SuKienAdapter(getContext(), this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        binding.rcvSuKien.setAdapter(adapter);
        binding.rcvSuKien.setLayoutManager(manager);
        getSuKien();
    }

    private void getSuKien() {
        database.child(username).child(Data.SU_KIEN).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String idSuKien = dataSnapshot.getKey();
                    SuKienModel suKienModel = dataSnapshot.getValue(SuKienModel.class);
                    SuKienWithIdModel suKienWithIdModel = new SuKienWithIdModel(idSuKien, suKienModel);
                    list.add(suKienWithIdModel);
                }
                list = Utils.sortSuKienByTime(list);
                showData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showData() {
        adapter.setData(list);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_su_kien;
    }

    @Override
    public void deleteSuKienListener(int pos) {
        if(!Utils.getInstance().isOnline(getContext())){
            Toast.makeText(getContext(), getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
            return;
        }
        database.child(username).child(Data.SU_KIEN).child(list.get(pos).getId()).setValue(null);
    }
}
