package com.leenguyen.studentnote.screens.main.model;

public class MonHocWithTbmModel {
    private String id;
    private String tenMonHoc;
    private int tbm;

    public MonHocWithTbmModel(String id, String tenMonHoc, int tbm) {
        this.tenMonHoc = tenMonHoc;
        this.tbm = tbm;
        this.id = id;
    }

    public MonHocWithTbmModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenMonHoc() {
        return tenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        this.tenMonHoc = tenMonHoc;
    }

    public int getTbm() {
        return tbm;
    }

    public void setTbm(int tbm) {
        this.tbm = tbm;
    }
}
