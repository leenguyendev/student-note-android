package com.leenguyen.studentnote.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.leenguyen.studentnote.R;

public class LoadingDialog {
    private static LoadingDialog instance;
    private Dialog dialogLoading;

    public static LoadingDialog getInstance() {
        if (instance == null) {
            instance = new LoadingDialog();
        }
        return instance;
    }

    public void showLoadingDialog(Context context) {
        if (dialogLoading != null) {
            dialogLoading = null;
        }
        dialogLoading = new Dialog(context);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading);
        ImageView imgLoading = dialogLoading.findViewById(R.id.imgLoading);
        Glide.with(context).load(R.drawable.ic_ami_quay).into(imgLoading);
        dialogLoading.setCancelable(false);
        dialogLoading.show();
    }

    public void hideLoadingDialog() {
        if (dialogLoading != null && dialogLoading.isShowing()) {
            dialogLoading.dismiss();
            dialogLoading = null;
        }
    }
}
