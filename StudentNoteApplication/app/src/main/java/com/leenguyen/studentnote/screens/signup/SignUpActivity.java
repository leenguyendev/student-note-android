package com.leenguyen.studentnote.screens.signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivitySignUpBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.screens.main.model.AccountModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends BaseActivity<ActivitySignUpBinding> {
    private boolean isNameOK = false, isUsernameOK = false, isPasswordOK = false;
    private String regex = "^[a-zA-Z0-9]+$";

    @Override
    protected void initLayout() {
        Pattern pattern = Pattern.compile(regex);
        binding.btnBack.setOnClickListener(v -> finish());
        binding.btnSignup.setOnClickListener(v -> signupNewAccount());
        binding.edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.txtNameError.setVisibility(s.length() > 0 ? View.GONE : View.VISIBLE);
                isNameOK = s.length() > 0;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.txtPasswordError.setVisibility(s.length() >= 8 ? View.GONE : View.VISIBLE);
                isPasswordOK = s.length() >= 8;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.edtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String username = binding.edtUsername.getText().toString();
                Matcher matcher = pattern.matcher(username);
                binding.txtUsernameError.setVisibility(matcher.matches() ? View.GONE : View.VISIBLE);
                isUsernameOK = matcher.matches();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void signupNewAccount() {
        if (checkInput()) {
            String name = binding.edtName.getText().toString();
            String username = binding.edtUsername.getText().toString();
            String password = binding.edtPassword.getText().toString();
            database.child(username).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        //Show notice dialog acc exist
                        NoticeDialog.getInstance().showNoticeDialog(SignUpActivity.this, getString(R.string.loi), getString(R.string.tai_khoan_da_ton_tai), null);
                    } else {
                        addNewAccount(new AccountModel(username, name, password));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void addNewAccount(AccountModel accountModel) {
        database.child(accountModel.getUsername()).child(Data.ACCOUNT).setValue(accountModel);
        NoticeDialog.getInstance().showNoticeDialog(this, getString(R.string.thang_cong), getString(R.string.dang_ky_tai_khoan_thanh_cong), null);
    }

    private boolean checkInput() {
        return isNameOK && isUsernameOK && isPasswordOK;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_sign_up;
    }
}