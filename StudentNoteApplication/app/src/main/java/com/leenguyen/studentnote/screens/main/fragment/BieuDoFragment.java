package com.leenguyen.studentnote.screens.main.fragment;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentBieuDoBinding;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.screens.main.ChangeClassListener;
import com.leenguyen.studentnote.screens.main.ChangeHocKyListener;
import com.leenguyen.studentnote.screens.main.ItemBieuDoClickListener;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.screens.main.adapter.BieuDoAdapter;
import com.leenguyen.studentnote.screens.main.model.MonHocWithTbmModel;
import com.leenguyen.studentnote.screens.monhocdetail.MonHocDetailActivity;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class BieuDoFragment extends BaseFragment<FragmentBieuDoBinding> implements ChangeClassListener, ChangeHocKyListener, ItemBieuDoClickListener {
    private String userName;
    private List<MonHocWithIDModel> list;
    private BieuDoAdapter adapter;

    @Override
    protected void initLayout() {
        ((MainActivity) getActivity()).setChangeClassBieuDoListener(this);
        ((MainActivity) getActivity()).setChangeHocKyBieuDoListener(this);
        userName = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        adapter = new BieuDoAdapter(getContext(), this);
        binding.rcvMonHoc.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        binding.rcvMonHoc.setLayoutManager(manager);
        getDanhSachMonHoc();
    }

    private void getDanhSachMonHoc() {
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        database.child(userName).child(lop).child(Data.MON_HOC).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String idMonHoc = dataSnapshot.getKey();
                    MonHocModel monHocModel = dataSnapshot.getValue(MonHocModel.class);
                    if (!monHocModel.getHe_so_mon().equals("0")) {
                        MonHocWithIDModel monHocWithIDModel = new MonHocWithIDModel(idMonHoc, monHocModel);
                        list.add(monHocWithIDModel);
                    }
                }
                createDanhSachMonHocDaSapXep(list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void createDanhSachMonHocDaSapXep(List<MonHocWithIDModel> list) {
        List<MonHocWithIDModel> listTemple = new ArrayList<>();
        ArrayList<MonHocWithTbmModel> monHocWithTbmModels = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            listTemple.clear();
            listTemple.add(list.get(i));
            String id = list.get(i).getId();
            String tenMonHoc = list.get(i).getMonHoc().getTen_mon_hoc();
            int diemTrungBinh = (int) (10 * Utils.getInstance().tinhDiemTrungBinhMon(getContext(), listTemple));
            MonHocWithTbmModel model = new MonHocWithTbmModel(id, tenMonHoc, diemTrungBinh);
            monHocWithTbmModels.add(model);
        }
        monHocWithTbmModels = Utils.sortMonHocByTbm(monHocWithTbmModels);
        showDataChart(monHocWithTbmModels);
        showListMonHoc(monHocWithTbmModels);
    }

    private void showListMonHoc(ArrayList<MonHocWithTbmModel> monHocWithTbmModels) {
        adapter.setData(monHocWithTbmModels);
    }

    private void showDataChart(ArrayList<MonHocWithTbmModel> monHocWithTbmModels) {
        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < monHocWithTbmModels.size(); i++) {
            entries.add(new BarEntry(i + 1, monHocWithTbmModels.get(i).getTbm()));
        }
        BarDataSet dataSet = new BarDataSet(entries, "Trung bình môn");
        dataSet.setValueTextSize(12);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData barData = new BarData(dataSet);

        binding.chart.setMaxVisibleValueCount(100);
        binding.chart.setData(barData);
        binding.chart.getDescription().setText("");
        binding.chart.getXAxis().setLabelCount(list.size());
        binding.chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        binding.chart.getXAxis().setTextSize(10);
        binding.chart.animateY(1000);
        binding.chart.invalidate();
        binding.chart.setPinchZoom(false);
        binding.chart.setDoubleTapToZoomEnabled(false);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_bieu_do;
    }

    @Override
    public void changeClassListener() {
        getDanhSachMonHoc();
    }

    @Override
    public void changeHocKyListener() {
        createDanhSachMonHocDaSapXep(list);
    }

    @Override
    public void itemBieuDoClickListener(String id) {
        Intent intent = new Intent(getContext(), MonHocDetailActivity.class);
        intent.putExtra(TongQuanFragment.KEY_ID_MON_HOC, id);
        startActivity(intent);
    }
}
