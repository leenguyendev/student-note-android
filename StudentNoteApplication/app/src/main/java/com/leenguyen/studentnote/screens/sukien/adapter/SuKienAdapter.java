package com.leenguyen.studentnote.screens.sukien.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.ItemSuKienBinding;
import com.leenguyen.studentnote.screens.sukien.DeleteSuKienListener;
import com.leenguyen.studentnote.screens.sukien.model.SuKienModel;
import com.leenguyen.studentnote.screens.sukien.model.SuKienWithIdModel;

import java.util.Calendar;
import java.util.List;

public class SuKienAdapter extends RecyclerView.Adapter<SuKienAdapter.SuKienViewHolder> {
    private Context context;
    private List<SuKienWithIdModel> list;
    private DeleteSuKienListener listener;

    public SuKienAdapter(Context context, DeleteSuKienListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<SuKienWithIdModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SuKienViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSuKienBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_su_kien, parent, false);
        return new SuKienViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SuKienViewHolder holder, int position) {
        holder.binding.txtNgay.setText(list.get(position).getSuKienModel().getThoiGian().substring(6, 8));
        holder.binding.txtTenSuKien.setText(list.get(position).getSuKienModel().getTenSuKien());
        holder.binding.txtBuoi.setText(list.get(position).getSuKienModel().getBuoi());
        holder.binding.txtDiaDiem.setText("Địa điểm: " + (list.get(position).getSuKienModel().getDiaDiem().equals("") ? "..." : list.get(position).getSuKienModel().getDiaDiem()));
        holder.binding.txtTiet.setText("Tiết " + list.get(position).getSuKienModel().getTiet());
        String thoiGian = list.get(position).getSuKienModel().getThoiGian();
        holder.binding.txtThoiGian.setText(thoiGian.substring(6, 8) + "-" + thoiGian.substring(4, 6) + "-" + thoiGian.substring(0, 4));
        Calendar calendar = Calendar.getInstance();
        int myDayOfMoth = calendar.get(Calendar.DAY_OF_MONTH);
        int myMonth = calendar.get(Calendar.MONTH) + 1;
        int myYear = calendar.get(Calendar.YEAR);
        String time = myYear + (myMonth < 10 ? "0" + myMonth : String.valueOf(myMonth)) + myDayOfMoth;
        if (time.equals(thoiGian)) {
            holder.binding.imgRemind.setVisibility(View.VISIBLE);
            Glide.with(context).load(R.drawable.ic_reminder_3).into(holder.binding.imgRemind);
        } else {
            holder.binding.imgRemind.setVisibility(View.GONE);
        }
        holder.binding.btnDelete.setOnClickListener(v -> listener.deleteSuKienListener(position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class SuKienViewHolder extends RecyclerView.ViewHolder {
        ItemSuKienBinding binding;

        public SuKienViewHolder(ItemSuKienBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
