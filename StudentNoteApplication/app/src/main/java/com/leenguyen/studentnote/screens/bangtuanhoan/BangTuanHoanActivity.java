package com.leenguyen.studentnote.screens.bangtuanhoan;

import com.bumptech.glide.Glide;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.databinding.ActivityBangTuanHoanBinding;

public class BangTuanHoanActivity extends BaseActivity<ActivityBangTuanHoanBinding> {

    @Override
    protected void initLayout() {
        binding.btnBack.setOnClickListener(v -> finish());
        Glide.with(this).load(R.drawable.bang_tuan_hoan).into(binding.img);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bang_tuan_hoan;
    }
}