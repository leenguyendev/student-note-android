package com.leenguyen.studentnote.screens.game;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.ItemOCoBinding;

public class CaroAdapter extends RecyclerView.Adapter<CaroAdapter.CaroViewHolder> {
    private final Context context;
    private OnItemOCoClick listener;
    private int[] mangQuanCo;

    public CaroAdapter(Context context, OnItemOCoClick listener) {
        this.context = context;
        this.listener = listener;
    }

    public void setData(int[] mangQuanCo) {
        this.mangQuanCo = mangQuanCo;
        notifyDataSetChanged();
    }

    public void setQuanCo(int pos, int quanCo) {
        if (mangQuanCo[pos] != CaroActivity.LUOT_X && mangQuanCo[pos] != CaroActivity.LUOT_O || quanCo == 0) {
            mangQuanCo[pos] = quanCo;
            notifyItemChanged(pos);
        }
    }

    @NonNull
    @Override
    public CaroAdapter.CaroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemOCoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_o_co, parent, false);
        return new CaroViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CaroViewHolder holder, int position) {
        switch (mangQuanCo[position]) {
            case CaroActivity.LUOT_X:
                holder.binding.imgQuanCo.setImageResource(R.drawable.ic_caro_x);
                break;
            case CaroActivity.LUOT_O:
                holder.binding.imgQuanCo.setImageResource(R.drawable.ic_caro_o);
                break;
            default:
                holder.binding.imgQuanCo.setImageResource(R.drawable.bg_o_co);
                break;
        }
        holder.binding.loOCo.setOnClickListener(v -> listener.onItemOCoClick(position));
    }

    @Override
    public int getItemCount() {
        return 225;
    }

    public static class CaroViewHolder extends RecyclerView.ViewHolder {
        ItemOCoBinding binding;

        public CaroViewHolder(ItemOCoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
