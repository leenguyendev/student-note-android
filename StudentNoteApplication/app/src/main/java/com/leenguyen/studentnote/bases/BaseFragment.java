package com.leenguyen.studentnote.bases;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.utils.Utils;

public abstract class BaseFragment<B extends ViewDataBinding> extends Fragment {
    protected B binding;
    protected DatabaseReference database;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutResourceId(), container, false);
        database = FirebaseDatabase.getInstance("https://student-note-hoc-sinh.firebaseio.com/").getReference();
        initLayout();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Utils.getInstance().isOnline(getContext())) {
            Toast.makeText(getContext(), getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
        }
    }

    protected abstract void initLayout();

    protected abstract int getLayoutResourceId();
}
