package com.leenguyen.studentnote.screens.game;

import androidx.recyclerview.widget.GridLayoutManager;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.databinding.ActivityCaroBinding;

public class CaroActivity extends BaseActivity<ActivityCaroBinding> implements OnItemOCoClick {
    public static final int LUOT_X = 1;
    public static final int LUOT_O = 2;
    private CaroAdapter adapter;
    private int[] mangQuanCo = new int[225];
    private int luotDanh = LUOT_X;
    private int previousPos = -1;


    @Override
    protected void initLayout() {
        binding.btnBack.setOnClickListener(v -> finish());
        binding.loNew.setOnClickListener(v -> taoVanMoi());
        binding.btnRollback.setOnClickListener(v -> luiMotBuoc());
        adapter = new CaroAdapter(this, this);
        binding.banCo.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(this, 15);
        binding.banCo.setLayoutManager(manager);
        adapter.setData(mangQuanCo);
        binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);
    }

    private void luiMotBuoc() {
        if (previousPos != -1) {
            adapter.setQuanCo(previousPos, 0);
            luotDanh = luotDanh == LUOT_X ? LUOT_O : LUOT_X;
            binding.imgLuotDanh.setImageResource(luotDanh == LUOT_X ? R.drawable.ic_caro_x : R.drawable.ic_caro_o);
            previousPos = -1;
        }
    }

    private void taoVanMoi() {
        mangQuanCo = new int[225];
        adapter.setData(mangQuanCo);
        luotDanh = LUOT_X;
        binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_caro;
    }

    @Override
    public void onItemOCoClick(int pos) {
        if (luotDanh == LUOT_X) {
            adapter.setQuanCo(pos, LUOT_X);
            luotDanh = LUOT_O;
            binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_o);
        } else {
            adapter.setQuanCo(pos, LUOT_O);
            luotDanh = LUOT_X;
            binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);
        }
        previousPos = pos;
    }
}