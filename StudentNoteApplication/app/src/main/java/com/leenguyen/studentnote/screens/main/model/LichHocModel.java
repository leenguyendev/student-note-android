package com.leenguyen.studentnote.screens.main.model;

public class LichHocModel {
    private String thu_2, thu_3, thu_4, thu_5, thu_6, thu_7, chu_nhat;

    public LichHocModel(String thu_2, String thu_3, String thu_4, String thu_5, String thu_6, String thu_7, String chu_nhat) {
        this.thu_2 = thu_2;
        this.thu_3 = thu_3;
        this.thu_4 = thu_4;
        this.thu_5 = thu_5;
        this.thu_6 = thu_6;
        this.thu_7 = thu_7;
        this.chu_nhat = chu_nhat;
    }

    public LichHocModel() {
    }

    public String getThu_2() {
        return thu_2;
    }

    public void setThu_2(String thu_2) {
        this.thu_2 = thu_2;
    }

    public String getThu_3() {
        return thu_3;
    }

    public void setThu_3(String thu_3) {
        this.thu_3 = thu_3;
    }

    public String getThu_4() {
        return thu_4;
    }

    public void setThu_4(String thu_4) {
        this.thu_4 = thu_4;
    }

    public String getThu_5() {
        return thu_5;
    }

    public void setThu_5(String thu_5) {
        this.thu_5 = thu_5;
    }

    public String getThu_6() {
        return thu_6;
    }

    public void setThu_6(String thu_6) {
        this.thu_6 = thu_6;
    }

    public String getThu_7() {
        return thu_7;
    }

    public void setThu_7(String thu_7) {
        this.thu_7 = thu_7;
    }

    public String getChu_nhat() {
        return chu_nhat;
    }

    public void setChu_nhat(String chu_nhat) {
        this.chu_nhat = chu_nhat;
    }

    @Override
    public String toString() {
        return "LichHocModel{" +
                "thu_2='" + thu_2 + '\'' +
                ", thu_3='" + thu_3 + '\'' +
                ", thu_4='" + thu_4 + '\'' +
                ", thu_5='" + thu_5 + '\'' +
                ", thu_6='" + thu_6 + '\'' +
                ", thu_7='" + thu_7 + '\'' +
                ", chu_nhat='" + chu_nhat + '\'' +
                '}';
    }
}
