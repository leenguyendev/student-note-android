package com.leenguyen.studentnote.screens.login;

import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivityLoginBinding;
import com.leenguyen.studentnote.dialogs.LoadingDialog;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.screens.signup.SignUpActivity;
import com.leenguyen.studentnote.utils.DataUtils;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> {

    @Override
    protected void initLayout() {
        binding.txtDangKy.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, SignUpActivity.class)));
        binding.btnLogin.setOnClickListener(v -> login());
    }

    private void login() {
        if (checkInput()) {
            LoadingDialog.getInstance().showLoadingDialog(this);
            String username = binding.edtUsername.getText().toString();
            String password = binding.edtPassword.getText().toString();
            database.child(username).child(Data.ACCOUNT).child(Data.PASSWORD).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() != null && password.equals(snapshot.getValue().toString())) {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        DataUtils.getInstance().saveValue(LoginActivity.this, Data.DATA, Data.KEY_USERNAME, username);
                        DataUtils.getInstance().saveValue(LoginActivity.this, Data.DATA, Data.KEY_IS_LOGIN, Data.VALUE_IS_LOGIN);
                        DataUtils.getInstance().saveValue(LoginActivity.this, Data.DATA, Data.KEY_PASSWORD, password);
                        DataUtils.getInstance().saveValue(LoginActivity.this, Data.DATA, Data.KEY_KY_HOC, Data.VALUE_KY_1);
                        finish();
                    } else {
                        binding.txtWrongAcc.setVisibility(View.VISIBLE);
                    }
                    LoadingDialog.getInstance().hideLoadingDialog();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private boolean checkInput() {
        return binding.edtUsername.getText().length() > 0
                && binding.edtPassword.getText().length() > 0;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }
}