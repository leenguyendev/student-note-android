package com.leenguyen.studentnote.screens.caidat.fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentMatKhauMoUngDungBinding;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.utils.DataUtils;

public class MatKhauMoUngDungFragment extends BaseFragment<FragmentMatKhauMoUngDungBinding> {
    private static final int TYPE_THEM_MA_BAO_MAT = 0;
    private static final int TYPE_XOA_MA_BAO_MAT = 1;
    private int type;
    private String maBaoMatDaLuu;
    private String matKhau;

    @Override
    protected void initLayout() {
        ((CaiDatActivity) getActivity()).setTitle(getString(R.string.mat_khau_mo_app));
        matKhau = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_PASSWORD);
        String value = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_IS_USE_MA_BAO_MAT);
        binding.switchOption.setChecked(value.equals(Data.VALUE_IS_USE_MA_BAO_MAT));

        binding.switchOption.setOnClickListener(v -> {
            Log.d("RAKAN", "here");
            binding.switchOption.setClickable(false);
            maBaoMatDaLuu = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_MA_BAO_MAT);
            binding.soThu1.setText("");
            binding.soThu2.setText("");
            binding.soThu3.setText("");
            binding.soThu4.setText("");
            binding.soThu1.requestFocus();
            binding.loThemMaBaoMat.setVisibility(View.VISIBLE);
            if (!binding.switchOption.isChecked()) {
                type = TYPE_XOA_MA_BAO_MAT;
                binding.txtTitleLayoutThemMa.setText(getString(R.string.nhap_ma_bao_mat_de_xac_nhan));
            } else {
                type = TYPE_THEM_MA_BAO_MAT;
                binding.txtTitleLayoutThemMa.setText(getString(R.string.nhap_ma_bao_mat_moi));
            }
        });

        binding.soThu1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu2.requestFocus();
                }
            }
        });

        binding.soThu2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu3.requestFocus();
                } else {
                    binding.soThu1.requestFocus();
                }
            }
        });

        binding.soThu3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu4.requestFocus();
                } else {
                    binding.soThu2.requestFocus();
                }
            }
        });

        binding.soThu4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    binding.soThu3.requestFocus();
                }
            }
        });

        binding.btnHuy.setOnClickListener(v -> {
            binding.switchOption.setChecked(!binding.switchOption.isChecked());
            binding.loThemMaBaoMat.setVisibility(View.GONE);
            binding.switchOption.setClickable(true);
        });

        binding.btnLuu.setOnClickListener(v -> {
            if (!matKhau.equals(binding.edtMatKhau.getText().toString())) {
                Toast.makeText(getContext(), getString(R.string.mat_khau_khong_dung), Toast.LENGTH_SHORT).show();
                return;
            }
            String so1 = binding.soThu1.getText().toString();
            String so2 = binding.soThu2.getText().toString();
            String so3 = binding.soThu3.getText().toString();
            String so4 = binding.soThu4.getText().toString();
            if (type == TYPE_THEM_MA_BAO_MAT) {
                try {
                    if (Integer.parseInt(so1) >= 0
                            && Integer.parseInt(so2) >= 0
                            && Integer.parseInt(so3) >= 0
                            && Integer.parseInt(so1) >= 0) {
                        DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_MA_BAO_MAT, (so1 + so2 + so3 + so4));
                        DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_IS_USE_MA_BAO_MAT, Data.VALUE_IS_USE_MA_BAO_MAT);
                        Toast.makeText(getContext(), getString(R.string.luu_ma_bao_mat_thanh_cong), Toast.LENGTH_SHORT).show();
                        binding.loThemMaBaoMat.setVisibility(View.GONE);
                        binding.switchOption.setClickable(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    binding.txtError.setVisibility(View.VISIBLE);
                }
            } else {
                String maXacNhan = so1 + so2 + so3 + so4;
                if (maXacNhan.equals(maBaoMatDaLuu)) {
                    DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_MA_BAO_MAT, "null");
                    DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_IS_USE_MA_BAO_MAT, Data.VALUE_IS_NOT_USE_MA_BAO_MAT);
                    binding.loThemMaBaoMat.setVisibility(View.GONE);
                    binding.switchOption.setClickable(true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.ma_bao_mat_khong_dung), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (binding.loThemMaBaoMat.getVisibility() == View.VISIBLE) {
            binding.switchOption.setChecked(!binding.switchOption.isChecked());
            binding.loThemMaBaoMat.setVisibility(View.GONE);
            binding.switchOption.setClickable(true);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_mat_khau_mo_ung_dung;
    }
}
