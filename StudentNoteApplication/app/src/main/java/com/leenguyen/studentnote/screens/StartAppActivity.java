package com.leenguyen.studentnote.screens;

import android.content.Intent;

import com.bumptech.glide.Glide;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivityStartAppBinding;
import com.leenguyen.studentnote.screens.login.LoginActivity;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.screens.nhapmabaomat.NhapMaBaoMatActivity;
import com.leenguyen.studentnote.utils.DataUtils;

import java.util.Timer;
import java.util.TimerTask;

public class StartAppActivity extends BaseActivity<ActivityStartAppBinding> {

    @Override
    protected void initLayout() {
        Glide.with(this).load(R.drawable.ic_ami_hello).into(binding.image);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                boolean isLogin = DataUtils.getInstance().getValue(StartAppActivity.this, Data.DATA, Data.KEY_IS_LOGIN).equals(Data.VALUE_IS_LOGIN);
                if (isLogin) {
                    boolean isUseMaBaoMat = DataUtils.getInstance().getValue(StartAppActivity.this, Data.DATA, Data.KEY_IS_USE_MA_BAO_MAT).equals(Data.VALUE_IS_USE_MA_BAO_MAT);
                    if (isUseMaBaoMat) {
                        startActivity(new Intent(StartAppActivity.this, NhapMaBaoMatActivity.class));
                    } else {
                        startActivity(new Intent(StartAppActivity.this, MainActivity.class));
                    }
                } else {
                    startActivity(new Intent(StartAppActivity.this, LoginActivity.class));
                }
                finish();
            }
        }, 2000);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_start_app;
    }
}