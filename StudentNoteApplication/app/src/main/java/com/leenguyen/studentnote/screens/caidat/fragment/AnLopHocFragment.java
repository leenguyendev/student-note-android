package com.leenguyen.studentnote.screens.caidat.fragment;

import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentAnLopHocBinding;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.screens.caidat.SaveClickListener;
import com.leenguyen.studentnote.screens.caidat.adapter.AnHienLopHocAdapter;
import com.leenguyen.studentnote.screens.caidat.model.AnHienLopHocModel;
import com.leenguyen.studentnote.utils.DataUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class AnLopHocFragment extends BaseFragment<FragmentAnLopHocBinding> implements SaveClickListener {
    private AnHienLopHocAdapter adapter;
    private List<AnHienLopHocModel> list;

    @Override
    protected void initLayout() {
        ((CaiDatActivity) getActivity()).setTitle(getString(R.string.an_lop_hoc));
        ((CaiDatActivity) getActivity()).setSaveClickListener(this);
        adapter = new AnHienLopHocAdapter(getContext());
        binding.rcvDanhSachLop.setAdapter(adapter);

        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        binding.rcvDanhSachLop.setLayoutManager(manager);

        list = new ArrayList<>();
        String[] classList = getResources().getStringArray(R.array.array_class);
        for (String s : classList) {
            AnHienLopHocModel model = new AnHienLopHocModel(s, true);
            list.add(model);
        }

        String viTriLopBiAn = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_LOP_BI_AN);
        if (!viTriLopBiAn.equals("null")) {
            StringTokenizer tokenizer = new StringTokenizer(viTriLopBiAn);
            while (tokenizer.countTokens() > 0) {
                int viTri = Integer.parseInt(tokenizer.nextToken());
                list.get(viTri).setDisplay(false);
            }
        }

        adapter.setData(list);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_an_lop_hoc;
    }

    @Override
    public void saveClickListener() {
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        list = adapter.getData();
        StringBuilder danhSachViTriBiAn = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).isDisplay()) {
                danhSachViTriBiAn.append(" ").append(i);
                if (i == Integer.parseInt(lop) - 1) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).isDisplay()) {
                            DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_CLASS, String.valueOf(j + 1));
                            break;
                        }
                    }
                }
            }
        }
        DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_LOP_BI_AN, danhSachViTriBiAn.toString().trim());
        Toast.makeText(getContext(), getString(R.string.cai_dat_an_lop_thanh_cong), Toast.LENGTH_SHORT).show();
    }
}
