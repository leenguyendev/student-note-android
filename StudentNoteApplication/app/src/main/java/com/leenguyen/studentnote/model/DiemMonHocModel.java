package com.leenguyen.studentnote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DiemMonHocModel implements Parcelable {
    private String diem_he_so_1, diem_he_so_2, diem_he_so_3, muc_tieu;

    public DiemMonHocModel(String diem_he_so_1, String diem_he_so_2, String diem_he_so_3, String muc_tieu) {
        this.diem_he_so_1 = diem_he_so_1;
        this.diem_he_so_2 = diem_he_so_2;
        this.diem_he_so_3 = diem_he_so_3;
        this.muc_tieu = muc_tieu;
    }

    public DiemMonHocModel() {
    }

    protected DiemMonHocModel(Parcel in) {
        diem_he_so_1 = in.readString();
        diem_he_so_2 = in.readString();
        diem_he_so_3 = in.readString();
        muc_tieu = in.readString();
    }

    public static final Creator<DiemMonHocModel> CREATOR = new Creator<DiemMonHocModel>() {
        @Override
        public DiemMonHocModel createFromParcel(Parcel in) {
            return new DiemMonHocModel(in);
        }

        @Override
        public DiemMonHocModel[] newArray(int size) {
            return new DiemMonHocModel[size];
        }
    };

    public String getMuc_tieu() {
        return muc_tieu;
    }

    public void setMuc_tieu(String muc_tieu) {
        this.muc_tieu = muc_tieu;
    }

    public String getDiem_he_so_2() {
        return diem_he_so_2;
    }

    public void setDiem_he_so_2(String diem_he_so_2) {
        this.diem_he_so_2 = diem_he_so_2;
    }

    public String getDiem_he_so_3() {
        return diem_he_so_3;
    }

    public void setDiem_he_so_3(String diem_he_so_3) {
        this.diem_he_so_3 = diem_he_so_3;
    }

    public String getDiem_he_so_1() {
        return diem_he_so_1;
    }

    public void setDiem_he_so_1(String diem_he_so_1) {
        this.diem_he_so_1 = diem_he_so_1;
    }

    @Override
    public String toString() {
        return "DiemMonHocModel{" +
                "diem_he_so_1='" + diem_he_so_1 + '\'' +
                ", diem_he_so_2='" + diem_he_so_2 + '\'' +
                ", diem_he_so_3='" + diem_he_so_3 + '\'' +
                ", muc_tieu='" + muc_tieu + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(diem_he_so_1);
        dest.writeString(diem_he_so_2);
        dest.writeString(diem_he_so_3);
        dest.writeString(muc_tieu);
    }
}
