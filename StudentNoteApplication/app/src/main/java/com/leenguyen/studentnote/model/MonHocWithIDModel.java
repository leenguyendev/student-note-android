package com.leenguyen.studentnote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MonHocWithIDModel implements Parcelable {
    private String id;
    private MonHocModel monHoc;

    public MonHocWithIDModel(String id, MonHocModel monHoc) {
        this.id = id;
        this.monHoc = monHoc;
    }

    public MonHocWithIDModel() {
    }

    protected MonHocWithIDModel(Parcel in) {
        id = in.readString();
    }

    public static final Creator<MonHocWithIDModel> CREATOR = new Creator<MonHocWithIDModel>() {
        @Override
        public MonHocWithIDModel createFromParcel(Parcel in) {
            return new MonHocWithIDModel(in);
        }

        @Override
        public MonHocWithIDModel[] newArray(int size) {
            return new MonHocWithIDModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MonHocModel getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(MonHocModel monHoc) {
        this.monHoc = monHoc;
    }

    @Override
    public String toString() {
        return "MonHocWithIDModel{" +
                "id='" + id + '\'' +
                ", monHoc=" + monHoc +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }
}
