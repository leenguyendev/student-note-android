package com.leenguyen.studentnote.screens.caidat.fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentDoiPasswordBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

public class DoiPasswordFragment extends BaseFragment<FragmentDoiPasswordBinding> {
    private String password = "", username;

    @Override
    protected void initLayout() {
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        password = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_PASSWORD);
        ((CaiDatActivity) getActivity()).setTitle(getString(R.string.doi_pass));
        binding.edtMatKhauMoi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.txtError.setVisibility(s.length() >= 8 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.btnUpdate.setOnClickListener(v -> updateMatKhau());
    }

    private void updateMatKhau() {
        if (!Utils.getInstance().isOnline(getContext())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.can_co_ket_noi_mang), null);
            return;
        }
        if (!password.equals(binding.edtMatKhauCu.getText().toString())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.mat_khau_cu_khong_dung), null);
            return;
        }
        if (binding.edtMatKhauMoi.getText().length() < 8) {
            binding.txtError.setVisibility(View.VISIBLE);
            return;
        }
        if (password.equals(binding.edtMatKhauMoi.getText().toString())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.pass_khong_doi), null);
            return;
        }
        database.child(username).child(Data.ACCOUNT).child(Data.PASSWORD).setValue(binding.edtMatKhauMoi.getText().toString().trim());
        binding.edtMatKhauCu.setText("");
        binding.edtMatKhauMoi.setText("");
        binding.txtError.setVisibility(View.GONE);
        Toast.makeText(getContext(), getString(R.string.doi_mat_khau_thanh_cong), Toast.LENGTH_SHORT).show();
        ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.MENU_POS);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_doi_password;
    }
}
