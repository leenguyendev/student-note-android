package com.leenguyen.studentnote.model;

public class TinhDiemHocKyModel {
    private double diemTrungBinh;
    private int mauSo;

    public TinhDiemHocKyModel(double diemTrungBinh, int mauSo) {
        this.diemTrungBinh = diemTrungBinh;
        this.mauSo = mauSo;
    }

    public TinhDiemHocKyModel() {
    }

    public double getDiemTrungBinh() {
        return diemTrungBinh;
    }

    public void setDiemTrungBinh(double diemTrungBinh) {
        this.diemTrungBinh = diemTrungBinh;
    }

    public int getMauSo() {
        return mauSo;
    }

    public void setMauSo(int mauSo) {
        this.mauSo = mauSo;
    }
}
