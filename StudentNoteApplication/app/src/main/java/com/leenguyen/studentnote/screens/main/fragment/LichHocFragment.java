package com.leenguyen.studentnote.screens.main.fragment;

import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.DialogThemLichHocBinding;
import com.leenguyen.studentnote.databinding.FragmentLichHocBinding;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.screens.main.model.LichHocModel;
import com.leenguyen.studentnote.screens.sukien.model.SuKienModel;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

public class LichHocFragment extends BaseFragment<FragmentLichHocBinding> {
    private String username;
    private ArrayList<String> listMonHocThu2, listMonHocThu3, listMonHocThu4, listMonHocThu5, listMonHocThu6, listMonHocThu7, listMonHocChuNhat;
    private String initLichHoc = "- - - - - - - - - - - - - - - - - -";

    @Override
    protected void initLayout() {
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        getLichHoc();
        getCheckSuKien();
        setLichHocThu2();
        setLichHocThu3();
        setLichHocThu4();
        setLichHocThu5();
        setLichHocThu6();
        setLichHocThu7();
        setLichHocChuNhat();
    }

    private void getCheckSuKien() {
        Calendar calendar = Calendar.getInstance();
        int myDayOfMoth = calendar.get(Calendar.DAY_OF_MONTH);
        int myMonth = calendar.get(Calendar.MONTH) + 1;
        int myYear = calendar.get(Calendar.YEAR);
        String thoiGian = myYear + (myMonth < 10 ? "0" + myMonth : String.valueOf(myMonth)) + myDayOfMoth;
        database.child(username).child(Data.SU_KIEN).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        SuKienModel suKienModel = dataSnapshot.getValue(SuKienModel.class);
                        String time = suKienModel.getThoiGian();
                        if (time.equals(thoiGian)) {
                            ((MainActivity) getActivity()).setIconSuKien(true);
                            break;
                        } else {
                            ((MainActivity) getActivity()).setIconSuKien(false);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setLichHocThu2() {
        binding.t2t1.setOnClickListener(v -> showDialogThemLichHoc(2, 1));
        binding.t2t2.setOnClickListener(v -> showDialogThemLichHoc(2, 2));
        binding.t2t3.setOnClickListener(v -> showDialogThemLichHoc(2, 3));
        binding.t2t4.setOnClickListener(v -> showDialogThemLichHoc(2, 4));
        binding.t2t5.setOnClickListener(v -> showDialogThemLichHoc(2, 5));
        binding.t2t6.setOnClickListener(v -> showDialogThemLichHoc(2, 6));
        binding.t2t7.setOnClickListener(v -> showDialogThemLichHoc(2, 7));
        binding.t2t8.setOnClickListener(v -> showDialogThemLichHoc(2, 8));
        binding.t2t9.setOnClickListener(v -> showDialogThemLichHoc(2, 9));
        binding.t2t10.setOnClickListener(v -> showDialogThemLichHoc(2, 10));
        binding.t2t11.setOnClickListener(v -> showDialogThemLichHoc(2, 11));
        binding.t2t12.setOnClickListener(v -> showDialogThemLichHoc(2, 12));
        binding.t2t13.setOnClickListener(v -> showDialogThemLichHoc(2, 13));
        binding.t2t14.setOnClickListener(v -> showDialogThemLichHoc(2, 14));
        binding.t2t15.setOnClickListener(v -> showDialogThemLichHoc(2, 15));
        binding.t2t16.setOnClickListener(v -> showDialogThemLichHoc(2, 16));
        binding.t2t17.setOnClickListener(v -> showDialogThemLichHoc(2, 17));
        binding.t2t18.setOnClickListener(v -> showDialogThemLichHoc(2, 18));
    }

    private void setLichHocThu3() {
        binding.t3t1.setOnClickListener(v -> showDialogThemLichHoc(3, 1));
        binding.t3t2.setOnClickListener(v -> showDialogThemLichHoc(3, 2));
        binding.t3t3.setOnClickListener(v -> showDialogThemLichHoc(3, 3));
        binding.t3t4.setOnClickListener(v -> showDialogThemLichHoc(3, 4));
        binding.t3t5.setOnClickListener(v -> showDialogThemLichHoc(3, 5));
        binding.t3t6.setOnClickListener(v -> showDialogThemLichHoc(3, 6));
        binding.t3t7.setOnClickListener(v -> showDialogThemLichHoc(3, 7));
        binding.t3t8.setOnClickListener(v -> showDialogThemLichHoc(3, 8));
        binding.t3t9.setOnClickListener(v -> showDialogThemLichHoc(3, 9));
        binding.t3t10.setOnClickListener(v -> showDialogThemLichHoc(3, 10));
        binding.t3t11.setOnClickListener(v -> showDialogThemLichHoc(3, 11));
        binding.t3t12.setOnClickListener(v -> showDialogThemLichHoc(3, 12));
        binding.t3t13.setOnClickListener(v -> showDialogThemLichHoc(3, 13));
        binding.t3t14.setOnClickListener(v -> showDialogThemLichHoc(3, 14));
        binding.t3t15.setOnClickListener(v -> showDialogThemLichHoc(3, 15));
        binding.t3t16.setOnClickListener(v -> showDialogThemLichHoc(3, 16));
        binding.t3t17.setOnClickListener(v -> showDialogThemLichHoc(3, 17));
        binding.t3t18.setOnClickListener(v -> showDialogThemLichHoc(3, 18));
    }

    private void setLichHocThu4() {
        binding.t4t1.setOnClickListener(v -> showDialogThemLichHoc(4, 1));
        binding.t4t2.setOnClickListener(v -> showDialogThemLichHoc(4, 2));
        binding.t4t3.setOnClickListener(v -> showDialogThemLichHoc(4, 3));
        binding.t4t4.setOnClickListener(v -> showDialogThemLichHoc(4, 4));
        binding.t4t5.setOnClickListener(v -> showDialogThemLichHoc(4, 5));
        binding.t4t6.setOnClickListener(v -> showDialogThemLichHoc(4, 6));
        binding.t4t7.setOnClickListener(v -> showDialogThemLichHoc(4, 7));
        binding.t4t8.setOnClickListener(v -> showDialogThemLichHoc(4, 8));
        binding.t4t9.setOnClickListener(v -> showDialogThemLichHoc(4, 9));
        binding.t4t10.setOnClickListener(v -> showDialogThemLichHoc(4, 10));
        binding.t4t11.setOnClickListener(v -> showDialogThemLichHoc(4, 11));
        binding.t4t12.setOnClickListener(v -> showDialogThemLichHoc(4, 12));
        binding.t4t13.setOnClickListener(v -> showDialogThemLichHoc(4, 13));
        binding.t4t14.setOnClickListener(v -> showDialogThemLichHoc(4, 14));
        binding.t4t15.setOnClickListener(v -> showDialogThemLichHoc(4, 15));
        binding.t4t16.setOnClickListener(v -> showDialogThemLichHoc(4, 16));
        binding.t4t17.setOnClickListener(v -> showDialogThemLichHoc(4, 17));
        binding.t4t18.setOnClickListener(v -> showDialogThemLichHoc(4, 18));
    }

    private void setLichHocThu5() {
        binding.t5t1.setOnClickListener(v -> showDialogThemLichHoc(5, 1));
        binding.t5t2.setOnClickListener(v -> showDialogThemLichHoc(5, 2));
        binding.t5t3.setOnClickListener(v -> showDialogThemLichHoc(5, 3));
        binding.t5t4.setOnClickListener(v -> showDialogThemLichHoc(5, 4));
        binding.t5t5.setOnClickListener(v -> showDialogThemLichHoc(5, 5));
        binding.t5t6.setOnClickListener(v -> showDialogThemLichHoc(5, 6));
        binding.t5t7.setOnClickListener(v -> showDialogThemLichHoc(5, 7));
        binding.t5t8.setOnClickListener(v -> showDialogThemLichHoc(5, 8));
        binding.t5t9.setOnClickListener(v -> showDialogThemLichHoc(5, 9));
        binding.t5t10.setOnClickListener(v -> showDialogThemLichHoc(5, 10));
        binding.t5t11.setOnClickListener(v -> showDialogThemLichHoc(5, 11));
        binding.t5t12.setOnClickListener(v -> showDialogThemLichHoc(5, 12));
        binding.t5t13.setOnClickListener(v -> showDialogThemLichHoc(5, 13));
        binding.t5t14.setOnClickListener(v -> showDialogThemLichHoc(5, 14));
        binding.t5t15.setOnClickListener(v -> showDialogThemLichHoc(5, 15));
        binding.t5t16.setOnClickListener(v -> showDialogThemLichHoc(5, 16));
        binding.t5t17.setOnClickListener(v -> showDialogThemLichHoc(5, 17));
        binding.t5t18.setOnClickListener(v -> showDialogThemLichHoc(5, 18));
    }

    private void setLichHocThu6() {
        binding.t6t1.setOnClickListener(v -> showDialogThemLichHoc(6, 1));
        binding.t6t2.setOnClickListener(v -> showDialogThemLichHoc(6, 2));
        binding.t6t3.setOnClickListener(v -> showDialogThemLichHoc(6, 3));
        binding.t6t4.setOnClickListener(v -> showDialogThemLichHoc(6, 4));
        binding.t6t5.setOnClickListener(v -> showDialogThemLichHoc(6, 5));
        binding.t6t6.setOnClickListener(v -> showDialogThemLichHoc(6, 6));
        binding.t6t7.setOnClickListener(v -> showDialogThemLichHoc(6, 7));
        binding.t6t8.setOnClickListener(v -> showDialogThemLichHoc(6, 8));
        binding.t6t9.setOnClickListener(v -> showDialogThemLichHoc(6, 9));
        binding.t6t10.setOnClickListener(v -> showDialogThemLichHoc(6, 10));
        binding.t6t11.setOnClickListener(v -> showDialogThemLichHoc(6, 11));
        binding.t6t12.setOnClickListener(v -> showDialogThemLichHoc(6, 12));
        binding.t6t13.setOnClickListener(v -> showDialogThemLichHoc(6, 13));
        binding.t6t14.setOnClickListener(v -> showDialogThemLichHoc(6, 14));
        binding.t6t15.setOnClickListener(v -> showDialogThemLichHoc(6, 15));
        binding.t6t16.setOnClickListener(v -> showDialogThemLichHoc(6, 16));
        binding.t6t17.setOnClickListener(v -> showDialogThemLichHoc(6, 17));
        binding.t6t18.setOnClickListener(v -> showDialogThemLichHoc(6, 18));
    }

    private void setLichHocThu7() {
        binding.t7t1.setOnClickListener(v -> showDialogThemLichHoc(7, 1));
        binding.t7t2.setOnClickListener(v -> showDialogThemLichHoc(7, 2));
        binding.t7t3.setOnClickListener(v -> showDialogThemLichHoc(7, 3));
        binding.t7t4.setOnClickListener(v -> showDialogThemLichHoc(7, 4));
        binding.t7t5.setOnClickListener(v -> showDialogThemLichHoc(7, 5));
        binding.t7t6.setOnClickListener(v -> showDialogThemLichHoc(7, 6));
        binding.t7t7.setOnClickListener(v -> showDialogThemLichHoc(7, 7));
        binding.t7t8.setOnClickListener(v -> showDialogThemLichHoc(7, 8));
        binding.t7t9.setOnClickListener(v -> showDialogThemLichHoc(7, 9));
        binding.t7t10.setOnClickListener(v -> showDialogThemLichHoc(7, 10));
        binding.t7t11.setOnClickListener(v -> showDialogThemLichHoc(7, 11));
        binding.t7t12.setOnClickListener(v -> showDialogThemLichHoc(7, 12));
        binding.t7t13.setOnClickListener(v -> showDialogThemLichHoc(7, 13));
        binding.t7t14.setOnClickListener(v -> showDialogThemLichHoc(7, 14));
        binding.t7t15.setOnClickListener(v -> showDialogThemLichHoc(7, 15));
        binding.t7t16.setOnClickListener(v -> showDialogThemLichHoc(7, 16));
        binding.t7t17.setOnClickListener(v -> showDialogThemLichHoc(7, 17));
        binding.t7t18.setOnClickListener(v -> showDialogThemLichHoc(7, 18));
    }

    private void setLichHocChuNhat() {
        binding.t8t1.setOnClickListener(v -> showDialogThemLichHoc(8, 1));
        binding.t8t2.setOnClickListener(v -> showDialogThemLichHoc(8, 2));
        binding.t8t3.setOnClickListener(v -> showDialogThemLichHoc(8, 3));
        binding.t8t4.setOnClickListener(v -> showDialogThemLichHoc(8, 4));
        binding.t8t5.setOnClickListener(v -> showDialogThemLichHoc(8, 5));
        binding.t8t6.setOnClickListener(v -> showDialogThemLichHoc(8, 6));
        binding.t8t7.setOnClickListener(v -> showDialogThemLichHoc(8, 7));
        binding.t8t8.setOnClickListener(v -> showDialogThemLichHoc(8, 8));
        binding.t8t9.setOnClickListener(v -> showDialogThemLichHoc(8, 9));
        binding.t8t10.setOnClickListener(v -> showDialogThemLichHoc(8, 10));
        binding.t8t11.setOnClickListener(v -> showDialogThemLichHoc(8, 11));
        binding.t8t12.setOnClickListener(v -> showDialogThemLichHoc(8, 12));
        binding.t8t13.setOnClickListener(v -> showDialogThemLichHoc(8, 13));
        binding.t8t14.setOnClickListener(v -> showDialogThemLichHoc(8, 14));
        binding.t8t15.setOnClickListener(v -> showDialogThemLichHoc(8, 15));
        binding.t8t16.setOnClickListener(v -> showDialogThemLichHoc(8, 16));
        binding.t8t17.setOnClickListener(v -> showDialogThemLichHoc(8, 17));
        binding.t8t18.setOnClickListener(v -> showDialogThemLichHoc(8, 18));
    }

    private void showDialogThemLichHoc(int thu, int tiet) {
        Dialog dialog = new Dialog(getContext());
        DialogThemLichHocBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_them_lich_hoc, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(false);
        bindingDialog.imgClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.edtMonHoc.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bindingDialog.txtDiemError.setVisibility(s.toString().contains(" ") ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bindingDialog.btnOk.setOnClickListener(v -> {
            if (!Utils.getInstance().isOnline(getContext())) {
                Toast.makeText(getContext(), getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
                return;
            }
            if (bindingDialog.txtDiemError.getVisibility() == View.VISIBLE) {
                return;
            }
            String key_thu = "";
            String monHoc = bindingDialog.edtMonHoc.getText().toString();
            StringBuilder builder = new StringBuilder();
            switch (thu) {
                case 2:
                    key_thu = "thu_2";
                    listMonHocThu2.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu2) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 3:
                    key_thu = "thu_3";
                    listMonHocThu3.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu3) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 4:
                    key_thu = "thu_4";
                    listMonHocThu4.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu4) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 5:
                    key_thu = "thu_5";
                    listMonHocThu5.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu5) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 6:
                    key_thu = "thu_6";
                    listMonHocThu6.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu6) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 7:
                    key_thu = "thu_7";
                    listMonHocThu7.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocThu7) {
                        builder.append(" ").append(mon);
                    }
                    break;
                case 8:
                    key_thu = "chu_nhat";
                    listMonHocChuNhat.set(tiet - 1, monHoc.equals("") ? "-" : monHoc);
                    for (String mon : listMonHocChuNhat) {
                        builder.append(" ").append(mon);
                    }
                    break;
            }
            database.child(username).child(Data.LICH_HOC).child(key_thu).setValue(String.valueOf(builder).trim());
            dialog.dismiss();
        });
        dialog.show();
    }

    private void getLichHoc() {
        database.child(username).child(Data.LICH_HOC).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    LichHocModel lichHocModel = snapshot.getValue(LichHocModel.class);
                    showLichHocThu2(lichHocModel.getThu_2() == null ? initLichHoc : lichHocModel.getThu_2());
                    showLichHocThu3(lichHocModel.getThu_3() == null ? initLichHoc : lichHocModel.getThu_3());
                    showLichHocThu4(lichHocModel.getThu_4() == null ? initLichHoc : lichHocModel.getThu_4());
                    showLichHocThu5(lichHocModel.getThu_5() == null ? initLichHoc : lichHocModel.getThu_5());
                    showLichHocThu6(lichHocModel.getThu_6() == null ? initLichHoc : lichHocModel.getThu_6());
                    showLichHocThu7(lichHocModel.getThu_7() == null ? initLichHoc : lichHocModel.getThu_7());
                    showLichHocThu8(lichHocModel.getChu_nhat() == null ? initLichHoc : lichHocModel.getChu_nhat());
                } else {
                    showLichHocThu2(initLichHoc);
                    showLichHocThu3(initLichHoc);
                    showLichHocThu4(initLichHoc);
                    showLichHocThu5(initLichHoc);
                    showLichHocThu6(initLichHoc);
                    showLichHocThu7(initLichHoc);
                    showLichHocThu8(initLichHoc);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showLichHocThu8(String tkbChuNhat) {
        if (tkbChuNhat != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbChuNhat);
            if (listMonHocChuNhat == null) {
                listMonHocChuNhat = new ArrayList<>();
            }
            listMonHocChuNhat.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocChuNhat.add(tokenizer.nextToken());
            }
            binding.t8t1.setText(listMonHocChuNhat.get(0).equals("-") ? "" : listMonHocChuNhat.get(0));
            binding.t8t2.setText(listMonHocChuNhat.get(1).equals("-") ? "" : listMonHocChuNhat.get(1));
            binding.t8t3.setText(listMonHocChuNhat.get(2).equals("-") ? "" : listMonHocChuNhat.get(2));
            binding.t8t4.setText(listMonHocChuNhat.get(3).equals("-") ? "" : listMonHocChuNhat.get(3));
            binding.t8t5.setText(listMonHocChuNhat.get(4).equals("-") ? "" : listMonHocChuNhat.get(4));
            binding.t8t6.setText(listMonHocChuNhat.get(5).equals("-") ? "" : listMonHocChuNhat.get(5));
            binding.t8t7.setText(listMonHocChuNhat.get(6).equals("-") ? "" : listMonHocChuNhat.get(6));
            binding.t8t8.setText(listMonHocChuNhat.get(7).equals("-") ? "" : listMonHocChuNhat.get(7));
            binding.t8t9.setText(listMonHocChuNhat.get(8).equals("-") ? "" : listMonHocChuNhat.get(8));
            binding.t8t10.setText(listMonHocChuNhat.get(9).equals("-") ? "" : listMonHocChuNhat.get(9));
            binding.t8t11.setText(listMonHocChuNhat.get(10).equals("-") ? "" : listMonHocChuNhat.get(10));
            binding.t8t12.setText(listMonHocChuNhat.get(11).equals("-") ? "" : listMonHocChuNhat.get(11));
            binding.t8t13.setText(listMonHocChuNhat.get(12).equals("-") ? "" : listMonHocChuNhat.get(12));
            binding.t8t14.setText(listMonHocChuNhat.get(13).equals("-") ? "" : listMonHocChuNhat.get(13));
            binding.t8t15.setText(listMonHocChuNhat.get(14).equals("-") ? "" : listMonHocChuNhat.get(14));
            binding.t8t16.setText(listMonHocChuNhat.get(15).equals("-") ? "" : listMonHocChuNhat.get(15));
            binding.t8t17.setText(listMonHocChuNhat.get(16).equals("-") ? "" : listMonHocChuNhat.get(16));
            binding.t8t18.setText(listMonHocChuNhat.get(17).equals("-") ? "" : listMonHocChuNhat.get(17));
        }
    }

    private void showLichHocThu7(String tkbThu7) {
        if (tkbThu7 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu7);
            if (listMonHocThu7 == null) {
                listMonHocThu7 = new ArrayList<>();
            }
            listMonHocThu7.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu7.add(tokenizer.nextToken());
            }
            binding.t7t1.setText(listMonHocThu7.get(0).equals("-") ? "" : listMonHocThu7.get(0));
            binding.t7t2.setText(listMonHocThu7.get(1).equals("-") ? "" : listMonHocThu7.get(1));
            binding.t7t3.setText(listMonHocThu7.get(2).equals("-") ? "" : listMonHocThu7.get(2));
            binding.t7t4.setText(listMonHocThu7.get(3).equals("-") ? "" : listMonHocThu7.get(3));
            binding.t7t5.setText(listMonHocThu7.get(4).equals("-") ? "" : listMonHocThu7.get(4));
            binding.t7t6.setText(listMonHocThu7.get(5).equals("-") ? "" : listMonHocThu7.get(5));
            binding.t7t7.setText(listMonHocThu7.get(6).equals("-") ? "" : listMonHocThu7.get(6));
            binding.t7t8.setText(listMonHocThu7.get(7).equals("-") ? "" : listMonHocThu7.get(7));
            binding.t7t9.setText(listMonHocThu7.get(8).equals("-") ? "" : listMonHocThu7.get(8));
            binding.t7t10.setText(listMonHocThu7.get(9).equals("-") ? "" : listMonHocThu7.get(9));
            binding.t7t11.setText(listMonHocThu7.get(10).equals("-") ? "" : listMonHocThu7.get(10));
            binding.t7t12.setText(listMonHocThu7.get(11).equals("-") ? "" : listMonHocThu7.get(11));
            binding.t7t13.setText(listMonHocThu7.get(12).equals("-") ? "" : listMonHocThu7.get(12));
            binding.t7t14.setText(listMonHocThu7.get(13).equals("-") ? "" : listMonHocThu7.get(13));
            binding.t7t15.setText(listMonHocThu7.get(14).equals("-") ? "" : listMonHocThu7.get(14));
            binding.t7t16.setText(listMonHocThu7.get(15).equals("-") ? "" : listMonHocThu7.get(15));
            binding.t7t17.setText(listMonHocThu7.get(16).equals("-") ? "" : listMonHocThu7.get(16));
            binding.t7t18.setText(listMonHocThu7.get(17).equals("-") ? "" : listMonHocThu7.get(17));
        }
    }

    private void showLichHocThu6(String tkbThu6) {
        if (tkbThu6 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu6);
            if (listMonHocThu6 == null) {
                listMonHocThu6 = new ArrayList<>();
            }
            listMonHocThu6.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu6.add(tokenizer.nextToken());
            }
            binding.t6t1.setText(listMonHocThu6.get(0).equals("-") ? "" : listMonHocThu6.get(0));
            binding.t6t2.setText(listMonHocThu6.get(1).equals("-") ? "" : listMonHocThu6.get(1));
            binding.t6t3.setText(listMonHocThu6.get(2).equals("-") ? "" : listMonHocThu6.get(2));
            binding.t6t4.setText(listMonHocThu6.get(3).equals("-") ? "" : listMonHocThu6.get(3));
            binding.t6t5.setText(listMonHocThu6.get(4).equals("-") ? "" : listMonHocThu6.get(4));
            binding.t6t6.setText(listMonHocThu6.get(5).equals("-") ? "" : listMonHocThu6.get(5));
            binding.t6t7.setText(listMonHocThu6.get(6).equals("-") ? "" : listMonHocThu6.get(6));
            binding.t6t8.setText(listMonHocThu6.get(7).equals("-") ? "" : listMonHocThu6.get(7));
            binding.t6t9.setText(listMonHocThu6.get(8).equals("-") ? "" : listMonHocThu6.get(8));
            binding.t6t10.setText(listMonHocThu6.get(9).equals("-") ? "" : listMonHocThu6.get(9));
            binding.t6t11.setText(listMonHocThu6.get(10).equals("-") ? "" : listMonHocThu6.get(10));
            binding.t6t12.setText(listMonHocThu6.get(11).equals("-") ? "" : listMonHocThu6.get(11));
            binding.t6t13.setText(listMonHocThu6.get(12).equals("-") ? "" : listMonHocThu6.get(12));
            binding.t6t14.setText(listMonHocThu6.get(13).equals("-") ? "" : listMonHocThu6.get(13));
            binding.t6t15.setText(listMonHocThu6.get(14).equals("-") ? "" : listMonHocThu6.get(14));
            binding.t6t16.setText(listMonHocThu6.get(15).equals("-") ? "" : listMonHocThu6.get(15));
            binding.t6t17.setText(listMonHocThu6.get(16).equals("-") ? "" : listMonHocThu6.get(16));
            binding.t6t18.setText(listMonHocThu6.get(17).equals("-") ? "" : listMonHocThu6.get(17));
        }
    }

    private void showLichHocThu5(String tkbThu5) {
        if (tkbThu5 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu5);
            if (listMonHocThu5 == null) {
                listMonHocThu5 = new ArrayList<>();
            }
            listMonHocThu5.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu5.add(tokenizer.nextToken());
            }
            binding.t5t1.setText(listMonHocThu5.get(0).equals("-") ? "" : listMonHocThu5.get(0));
            binding.t5t2.setText(listMonHocThu5.get(1).equals("-") ? "" : listMonHocThu5.get(1));
            binding.t5t3.setText(listMonHocThu5.get(2).equals("-") ? "" : listMonHocThu5.get(2));
            binding.t5t4.setText(listMonHocThu5.get(3).equals("-") ? "" : listMonHocThu5.get(3));
            binding.t5t5.setText(listMonHocThu5.get(4).equals("-") ? "" : listMonHocThu5.get(4));
            binding.t5t6.setText(listMonHocThu5.get(5).equals("-") ? "" : listMonHocThu5.get(5));
            binding.t5t7.setText(listMonHocThu5.get(6).equals("-") ? "" : listMonHocThu5.get(6));
            binding.t5t8.setText(listMonHocThu5.get(7).equals("-") ? "" : listMonHocThu5.get(7));
            binding.t5t9.setText(listMonHocThu5.get(8).equals("-") ? "" : listMonHocThu5.get(8));
            binding.t5t10.setText(listMonHocThu5.get(9).equals("-") ? "" : listMonHocThu5.get(9));
            binding.t5t11.setText(listMonHocThu5.get(10).equals("-") ? "" : listMonHocThu5.get(10));
            binding.t5t12.setText(listMonHocThu5.get(11).equals("-") ? "" : listMonHocThu5.get(11));
            binding.t5t13.setText(listMonHocThu5.get(12).equals("-") ? "" : listMonHocThu5.get(12));
            binding.t5t14.setText(listMonHocThu5.get(13).equals("-") ? "" : listMonHocThu5.get(13));
            binding.t5t15.setText(listMonHocThu5.get(14).equals("-") ? "" : listMonHocThu5.get(14));
            binding.t5t16.setText(listMonHocThu5.get(15).equals("-") ? "" : listMonHocThu5.get(15));
            binding.t5t17.setText(listMonHocThu5.get(16).equals("-") ? "" : listMonHocThu5.get(16));
            binding.t5t18.setText(listMonHocThu5.get(17).equals("-") ? "" : listMonHocThu5.get(17));
        }
    }

    private void showLichHocThu4(String tkbThu4) {
        if (tkbThu4 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu4);
            if (listMonHocThu4 == null) {
                listMonHocThu4 = new ArrayList<>();
            }
            listMonHocThu4.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu4.add(tokenizer.nextToken());
            }
            binding.t4t1.setText(listMonHocThu4.get(0).equals("-") ? "" : listMonHocThu4.get(0));
            binding.t4t2.setText(listMonHocThu4.get(1).equals("-") ? "" : listMonHocThu4.get(1));
            binding.t4t3.setText(listMonHocThu4.get(2).equals("-") ? "" : listMonHocThu4.get(2));
            binding.t4t4.setText(listMonHocThu4.get(3).equals("-") ? "" : listMonHocThu4.get(3));
            binding.t4t5.setText(listMonHocThu4.get(4).equals("-") ? "" : listMonHocThu4.get(4));
            binding.t4t6.setText(listMonHocThu4.get(5).equals("-") ? "" : listMonHocThu4.get(5));
            binding.t4t7.setText(listMonHocThu4.get(6).equals("-") ? "" : listMonHocThu4.get(6));
            binding.t4t8.setText(listMonHocThu4.get(7).equals("-") ? "" : listMonHocThu4.get(7));
            binding.t4t9.setText(listMonHocThu4.get(8).equals("-") ? "" : listMonHocThu4.get(8));
            binding.t4t10.setText(listMonHocThu4.get(9).equals("-") ? "" : listMonHocThu4.get(9));
            binding.t4t11.setText(listMonHocThu4.get(10).equals("-") ? "" : listMonHocThu4.get(10));
            binding.t4t12.setText(listMonHocThu4.get(11).equals("-") ? "" : listMonHocThu4.get(11));
            binding.t4t13.setText(listMonHocThu4.get(12).equals("-") ? "" : listMonHocThu4.get(12));
            binding.t4t14.setText(listMonHocThu4.get(13).equals("-") ? "" : listMonHocThu4.get(13));
            binding.t4t15.setText(listMonHocThu4.get(14).equals("-") ? "" : listMonHocThu4.get(14));
            binding.t4t16.setText(listMonHocThu4.get(15).equals("-") ? "" : listMonHocThu4.get(15));
            binding.t4t17.setText(listMonHocThu4.get(16).equals("-") ? "" : listMonHocThu4.get(16));
            binding.t4t18.setText(listMonHocThu4.get(17).equals("-") ? "" : listMonHocThu4.get(17));
        }
    }

    private void showLichHocThu3(String tkbThu3) {
        if (tkbThu3 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu3);
            if (listMonHocThu3 == null) {
                listMonHocThu3 = new ArrayList<>();
            }
            listMonHocThu3.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu3.add(tokenizer.nextToken());
            }
            binding.t3t1.setText(listMonHocThu3.get(0).equals("-") ? "" : listMonHocThu3.get(0));
            binding.t3t2.setText(listMonHocThu3.get(1).equals("-") ? "" : listMonHocThu3.get(1));
            binding.t3t3.setText(listMonHocThu3.get(2).equals("-") ? "" : listMonHocThu3.get(2));
            binding.t3t4.setText(listMonHocThu3.get(3).equals("-") ? "" : listMonHocThu3.get(3));
            binding.t3t5.setText(listMonHocThu3.get(4).equals("-") ? "" : listMonHocThu3.get(4));
            binding.t3t6.setText(listMonHocThu3.get(5).equals("-") ? "" : listMonHocThu3.get(5));
            binding.t3t7.setText(listMonHocThu3.get(6).equals("-") ? "" : listMonHocThu3.get(6));
            binding.t3t8.setText(listMonHocThu3.get(7).equals("-") ? "" : listMonHocThu3.get(7));
            binding.t3t9.setText(listMonHocThu3.get(8).equals("-") ? "" : listMonHocThu3.get(8));
            binding.t3t10.setText(listMonHocThu3.get(9).equals("-") ? "" : listMonHocThu3.get(9));
            binding.t3t11.setText(listMonHocThu3.get(10).equals("-") ? "" : listMonHocThu3.get(10));
            binding.t3t12.setText(listMonHocThu3.get(11).equals("-") ? "" : listMonHocThu3.get(11));
            binding.t3t13.setText(listMonHocThu3.get(12).equals("-") ? "" : listMonHocThu3.get(12));
            binding.t3t14.setText(listMonHocThu3.get(13).equals("-") ? "" : listMonHocThu3.get(13));
            binding.t3t15.setText(listMonHocThu3.get(14).equals("-") ? "" : listMonHocThu3.get(14));
            binding.t3t16.setText(listMonHocThu3.get(15).equals("-") ? "" : listMonHocThu3.get(15));
            binding.t3t17.setText(listMonHocThu3.get(16).equals("-") ? "" : listMonHocThu3.get(16));
            binding.t3t18.setText(listMonHocThu3.get(17).equals("-") ? "" : listMonHocThu3.get(17));
        }
    }

    private void showLichHocThu2(String tkbThu2) {
        if (tkbThu2 != null) {
            StringTokenizer tokenizer = new StringTokenizer(tkbThu2);
            if (listMonHocThu2 == null) {
                listMonHocThu2 = new ArrayList<>();
            }
            listMonHocThu2.clear();
            while (tokenizer.countTokens() > 0) {
                listMonHocThu2.add(tokenizer.nextToken());
            }
            binding.t2t1.setText(listMonHocThu2.get(0).equals("-") ? "" : listMonHocThu2.get(0));
            binding.t2t2.setText(listMonHocThu2.get(1).equals("-") ? "" : listMonHocThu2.get(1));
            binding.t2t3.setText(listMonHocThu2.get(2).equals("-") ? "" : listMonHocThu2.get(2));
            binding.t2t4.setText(listMonHocThu2.get(3).equals("-") ? "" : listMonHocThu2.get(3));
            binding.t2t5.setText(listMonHocThu2.get(4).equals("-") ? "" : listMonHocThu2.get(4));
            binding.t2t6.setText(listMonHocThu2.get(5).equals("-") ? "" : listMonHocThu2.get(5));
            binding.t2t7.setText(listMonHocThu2.get(6).equals("-") ? "" : listMonHocThu2.get(6));
            binding.t2t8.setText(listMonHocThu2.get(7).equals("-") ? "" : listMonHocThu2.get(7));
            binding.t2t9.setText(listMonHocThu2.get(8).equals("-") ? "" : listMonHocThu2.get(8));
            binding.t2t10.setText(listMonHocThu2.get(9).equals("-") ? "" : listMonHocThu2.get(9));
            binding.t2t11.setText(listMonHocThu2.get(10).equals("-") ? "" : listMonHocThu2.get(10));
            binding.t2t12.setText(listMonHocThu2.get(11).equals("-") ? "" : listMonHocThu2.get(11));
            binding.t2t13.setText(listMonHocThu2.get(12).equals("-") ? "" : listMonHocThu2.get(12));
            binding.t2t14.setText(listMonHocThu2.get(13).equals("-") ? "" : listMonHocThu2.get(13));
            binding.t2t15.setText(listMonHocThu2.get(14).equals("-") ? "" : listMonHocThu2.get(14));
            binding.t2t16.setText(listMonHocThu2.get(15).equals("-") ? "" : listMonHocThu2.get(15));
            binding.t2t17.setText(listMonHocThu2.get(16).equals("-") ? "" : listMonHocThu2.get(16));
            binding.t2t18.setText(listMonHocThu2.get(17).equals("-") ? "" : listMonHocThu2.get(17));
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_lich_hoc;
    }
}
