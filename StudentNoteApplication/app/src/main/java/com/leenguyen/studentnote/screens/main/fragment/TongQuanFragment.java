package com.leenguyen.studentnote.screens.main.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.DialogHanhKiemBinding;
import com.leenguyen.studentnote.databinding.FragmentTongQuanBinding;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.screens.main.ChangeClassListener;
import com.leenguyen.studentnote.screens.main.ChangeHocKyListener;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.screens.main.adapter.TongQuanAdapter;
import com.leenguyen.studentnote.screens.monhocdetail.MonHocDetailActivity;
import com.leenguyen.studentnote.screens.themmonhoc.ThemMonHocActivity;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;

public class TongQuanFragment extends BaseFragment<FragmentTongQuanBinding> implements ChangeClassListener, ChangeHocKyListener, ItemMonHocClickListener {
    public static final String KEY_ID_MON_HOC = "id";
    private String userName;
    private ArrayList<MonHocWithIDModel> list;
    private TongQuanAdapter adapter;

    @Override
    protected void initLayout() {
        ((MainActivity) getActivity()).setChangeClassTongQuanListener(this);
        ((MainActivity) getActivity()).setChangeHocKyTongQuanListener(this);

        userName = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);

        adapter = new TongQuanAdapter(getContext());
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        binding.rcvMonHoc.setLayoutManager(manager);
        binding.rcvMonHoc.setAdapter(adapter);
        adapter.setListener(this);

        binding.fab.setOnClickListener(v -> {
            //Intent to them mon hoc
            startActivity(new Intent(getContext(), ThemMonHocActivity.class));
        });
        binding.loHanhKiem.setOnClickListener(v -> {
            showDialogHanhKiem();
        });

        getDanhSachMonHoc();
        getHanhKiem();
    }

    private void getHanhKiem() {
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        String hocKy = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_KY_HOC);
        if (hocKy.equals(Data.VALUE_KY_1)) {
            hocKy = Data.HOC_KY_1;
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            hocKy = Data.HOC_KY_2;
        } else {
            hocKy = Data.TONG_KET;
        }
        database.child(userName).child(lop).child(Data.HANH_KIEM).child(hocKy).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    switch (snapshot.getValue(String.class)) {
                        case Data.VALUE_TOT:
                            binding.txtHanhKiem.setText(getString(R.string.tot));
                            break;
                        case Data.VALUE_KHA:
                            binding.txtHanhKiem.setText(getString(R.string.kha));
                            break;
                        case Data.VALUE_TRUNG_BINH:
                            binding.txtHanhKiem.setText(getString(R.string.trung_binh));
                            break;
                        case Data.VALUE_YEU:
                            binding.txtHanhKiem.setText(getString(R.string.yeu));
                            break;
                        case "":
                            binding.txtHanhKiem.setText("-:-");
                            break;
                    }
                    DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_HANH_KIEM, snapshot.getValue(String.class));
                } else {
                    binding.txtHanhKiem.setText("-:-");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void showDialogHanhKiem() {
        String hocKy = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_KY_HOC);
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        Dialog dialog = new Dialog(getContext());
        DialogHanhKiemBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_hanh_kiem, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(false);
        switch (hocKy) {
            case Data.VALUE_TONG_KET:
                bindingDialog.txtTitle.setText(getString(R.string.hanh_kiem_ca_nam));
                break;
            case Data.VALUE_KY_1:
                bindingDialog.txtTitle.setText(getString(R.string.hanh_kiem) + " kỳ 1");
                break;
            case Data.VALUE_KY_2:
                bindingDialog.txtTitle.setText(getString(R.string.hanh_kiem) + " kỳ 2");
                break;
        }
        String hanhKiem = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_HANH_KIEM);
        switch (hanhKiem) {
            case Data.VALUE_TOT:
                bindingDialog.rdbTot.setChecked(true);
                Glide.with(getContext()).load(R.drawable.ic_ami_love_2).into(bindingDialog.imgSticker);
                break;
            case Data.VALUE_KHA:
                bindingDialog.rdbKha.setChecked(true);
                Glide.with(getContext()).load(R.drawable.ic_ami_quay_2).into(bindingDialog.imgSticker);
                break;
            case Data.VALUE_TRUNG_BINH:
                bindingDialog.rdbTrungBinh.setChecked(true);
                Glide.with(getContext()).load(R.drawable.ic_ami_cry_2).into(bindingDialog.imgSticker);
                break;
            case Data.VALUE_YEU:
                bindingDialog.rdbYeu.setChecked(true);
                Glide.with(getContext()).load(R.drawable.ic_ami_cry).into(bindingDialog.imgSticker);
                break;
        }
        bindingDialog.rdbTot.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Glide.with(getContext()).load(R.drawable.ic_ami_love_2).into(bindingDialog.imgSticker);
            }
        });
        bindingDialog.rdbKha.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Glide.with(getContext()).load(R.drawable.ic_ami_quay_2).into(bindingDialog.imgSticker);
            }
        });
        bindingDialog.rdbTrungBinh.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Glide.with(getContext()).load(R.drawable.ic_ami_cry_2).into(bindingDialog.imgSticker);
            }
        });
        bindingDialog.rdbYeu.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Glide.with(getContext()).load(R.drawable.ic_ami_cry).into(bindingDialog.imgSticker);
            }
        });

        String finalHocKy;
        if (hocKy.equals(Data.VALUE_KY_1)) {
            finalHocKy = Data.HOC_KY_1;
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            finalHocKy = Data.HOC_KY_2;
        } else {
            finalHocKy = Data.TONG_KET;
        }
        bindingDialog.btnOk.setOnClickListener(v -> {
            if (!Utils.getInstance().isOnline(getContext())) {
                Toast.makeText(getContext(), getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
                return;
            }
            String hanhKiemNew;
            if (bindingDialog.rdbTot.isChecked()) {
                hanhKiemNew = Data.VALUE_TOT;
            } else if (bindingDialog.rdbKha.isChecked()) {
                hanhKiemNew = Data.VALUE_KHA;
            } else if (bindingDialog.rdbTrungBinh.isChecked()) {
                hanhKiemNew = Data.VALUE_TRUNG_BINH;
            } else if (bindingDialog.rdbYeu.isChecked()) {
                hanhKiemNew = Data.VALUE_YEU;
            } else {
                hanhKiemNew = "";
            }
            database.child(userName).child(lop).child(Data.HANH_KIEM).child(finalHocKy).setValue(hanhKiemNew);
            dialog.dismiss();
        });
        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void getDanhSachMonHoc() {
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        database.child(userName).child(lop).child(Data.MON_HOC).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String idMonHoc = dataSnapshot.getKey();
                    MonHocModel monHocModel = dataSnapshot.getValue(MonHocModel.class);
                    MonHocWithIDModel monHocWithIDModel = new MonHocWithIDModel(idMonHoc, monHocModel);
                    list.add(monHocWithIDModel);
                }
                if (list.size() > 0) {
                    binding.loHuongDanThemMon.setVisibility(View.GONE);
                } else {
                    Glide.with(requireContext()).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                    binding.loHuongDanThemMon.setVisibility(View.VISIBLE);
                }
                showData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showData() {
        adapter.setData(list);
        binding.txtTbm.setText(String.valueOf(Utils.getInstance().tinhDiemTrungBinhMon(getContext(), list)));
        showHocLuc();
    }

    private void showHocLuc() {
        String tbm = binding.txtTbm.getText().toString().trim();
        double diemTbm = Double.parseDouble(tbm);
        if (diemTbm >= 8) {
            binding.txtHocLuc.setText(getString(R.string.gioi));
        } else if (diemTbm >= 6.5) {
            binding.txtHocLuc.setText(getString(R.string.kha));
        } else if (diemTbm >= 5) {
            binding.txtHocLuc.setText(getString(R.string.trung_binh));
        } else if (diemTbm >= 3.5) {
            binding.txtHocLuc.setText(getString(R.string.yeu));
        } else {
            if (diemTbm != 0) {
                binding.txtHocLuc.setText(getString(R.string.kem));
            } else {
                binding.txtHocLuc.setText("-:-");
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_tong_quan;
    }

    @Override
    public void changeClassListener() {
        getDanhSachMonHoc();
        getHanhKiem();
    }

    @Override
    public void changeHocKyListener() {
        showData();
        getHanhKiem();
    }

    @Override
    public void itemClickListener(int position) {
        Intent intent = new Intent(getContext(), MonHocDetailActivity.class);
        intent.putExtra(KEY_ID_MON_HOC, list.get(position).getId());
        startActivity(intent);
    }
}