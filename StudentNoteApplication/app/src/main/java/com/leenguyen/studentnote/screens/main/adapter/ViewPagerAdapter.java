package com.leenguyen.studentnote.screens.main.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.leenguyen.studentnote.screens.main.fragment.BangDiemFragment;
import com.leenguyen.studentnote.screens.main.fragment.BieuDoFragment;
import com.leenguyen.studentnote.screens.main.fragment.LichHocFragment;
import com.leenguyen.studentnote.screens.main.fragment.TongQuanFragment;

public class ViewPagerAdapter extends FragmentStateAdapter {

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new TongQuanFragment();
            case 1:
                return new LichHocFragment();
            case 2:
                return new BangDiemFragment();
            case 3:
                return new BieuDoFragment();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
