package com.leenguyen.studentnote.screens.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager2.widget.ViewPager2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.review.testing.FakeReviewManager;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivityMainBinding;
import com.leenguyen.studentnote.databinding.DialogChonHocKyBinding;
import com.leenguyen.studentnote.databinding.DialogRotateImageBinding;
import com.leenguyen.studentnote.databinding.NavigationHeaderBinding;
import com.leenguyen.studentnote.screens.bangtuanhoan.BangTuanHoanActivity;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.screens.game.CaroActivity;
import com.leenguyen.studentnote.screens.login.LoginActivity;
import com.leenguyen.studentnote.screens.main.adapter.ViewPagerAdapter;
import com.leenguyen.studentnote.screens.sukien.SuKienActivity;
import com.leenguyen.studentnote.utils.DataUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements NavigationView.OnNavigationItemSelectedListener {
    public static final int PICK_IMAGE = 1;
    private static final int LICH_HOC_POS = 1;
    private NavigationHeaderBinding navBinding;
    private int currentPosSelected;
    private String[] classList;
    private ViewPagerAdapter viewPagerAdapter;
    private ChangeClassListener tongQuanListener, bangDiemListener, bieuDoListener;
    private ChangeHocKyListener changeHocKyTongQuanListener, changeHocKyBangDiemListener, changeHocKyBieuDoListener;
    private float rotateAngle;
    private ReviewInfo reviewInfo;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void initLayout() {
        setSupportActionBar(binding.toolBar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.toolBar, R.string.open_nav, R.string.close_nav);
        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        binding.navigation.setNavigationItemSelectedListener(this);

        navBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.navigation_header, binding.navigation, true);
        classList = getResources().getStringArray(R.array.array_class);

        viewPagerAdapter = new ViewPagerAdapter(this);
        binding.fragment.setAdapter(viewPagerAdapter);
        binding.bottomBar.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.tong_quan:
                    binding.fragment.setCurrentItem(0);
                    break;
                case R.id.lich_hoc:
                    binding.fragment.setCurrentItem(1);
                    break;
                case R.id.bang_diem:
                    binding.fragment.setCurrentItem(2);
                    break;
                case R.id.bieu_do:
                    binding.fragment.setCurrentItem(3);
                    break;
            }
            return true;
        });
        binding.fragment.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                binding.bottomBar.getMenu().getItem(position).setChecked(true);
                binding.imgSetKyHoc.setVisibility(position == LICH_HOC_POS ? View.GONE : View.VISIBLE);
                binding.imgSuKien.setVisibility(position == LICH_HOC_POS ? View.VISIBLE : View.GONE);
            }
        });
        binding.fragment.setOffscreenPageLimit(4);

        binding.imgSetKyHoc.setOnClickListener(v -> {
            //Show dialog set ky hoc
            showChonHocKyDialog();
        });

        binding.imgSuKien.setOnClickListener(v -> startActivity(new Intent(this, SuKienActivity.class)));

        navBinding.imgLogout.setOnClickListener(v -> logout());

        navBinding.imgAvatar.setOnClickListener(v -> {
            if (isStoragePermissionGranted()) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
        displayAvatar();
        initUserInfo();
        inAppReview();
    }

    private void inAppReview() {
        ReviewManager manager = ReviewManagerFactory.create(this);
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reviewInfo = task.getResult();
            }
        });
        if (reviewInfo != null) {
            Task<Void> flow = manager.launchReviewFlow(this, reviewInfo);
            flow.addOnCompleteListener(task -> {

            });
        }
    }

    private void displayAvatar() {
        String encodedImage = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_AVATAR);
        String rotateAngle = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_AVATAR_ROTATE_ANGLE);
        if (!encodedImage.equals("null") && !encodedImage.equals("null")) {
            byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
            navBinding.imgAvatar.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            navBinding.imgAvatar.setRotation(Float.parseFloat(rotateAngle));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            if (requestCode == PICK_IMAGE && data.getData() != null) {
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    showDialogEditImage(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showDialogEditImage(final Bitmap bitmap) {

        final Dialog dialog = new Dialog(this);
        final DialogRotateImageBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_rotate_image, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.image.setImageBitmap(bitmap);

        rotateAngle = 0;
        bindingDialog.rolLeft.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() - 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() - 90);
        });

        bindingDialog.rolRight.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() + 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() + 90);
        });

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.btnSave.setOnClickListener(view -> {
            navBinding.imgAvatar.setImageBitmap(bitmap);
            navBinding.imgAvatar.setRotation(rotateAngle % 360);
            bindingDialog.btnSave.setClickable(false);
            bindingDialog.llRotate.setClickable(false);
//              Save
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_AVATAR, encodedImage);
            DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_AVATAR_ROTATE_ANGLE, String.valueOf(rotateAngle % 360));
            dialog.dismiss();
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    @SuppressLint("ObsoleteSdkInt")
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkReadWriteExternalStorePermission()) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private boolean checkReadWriteExternalStorePermission() {
        int write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void showChonHocKyDialog() {
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        Dialog dialog = new Dialog(this);
        DialogChonHocKyBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_chon_hoc_ky, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(true);
        switch (hocKy) {
            case "null":
            case Data.VALUE_TONG_KET:
                bindingDialog.rdbTongKet.setChecked(true);
                break;
            case Data.VALUE_KY_1:
                bindingDialog.rdbKy1.setChecked(true);
                break;
            case Data.VALUE_KY_2:
                bindingDialog.rdbKy2.setChecked(true);
                break;
        }

        bindingDialog.btnOk.setOnClickListener(v -> {
            String hocky2;
            if (bindingDialog.rdbKy1.isChecked()) {
                hocky2 = Data.VALUE_KY_1;
            } else if (bindingDialog.rdbKy2.isChecked()) {
                hocky2 = Data.VALUE_KY_2;
            } else {
                hocky2 = Data.VALUE_TONG_KET;
            }
            DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_KY_HOC, hocky2);
            changeHocKyTongQuanListener.changeHocKyListener();
            changeHocKyBangDiemListener.changeHocKyListener();
            changeHocKyBieuDoListener.changeHocKyListener();
            setTitle(currentPosSelected);
            dialog.dismiss();
        });
        dialog.show();
    }

    private void logout() {
        //Delete SharedPreferences data
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_NAME, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_CLASS, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_USERNAME, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_PASSWORD, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_AVATAR, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_AVATAR_ROTATE_ANGLE, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_IS_LOGIN, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_IS_USE_MA_BAO_MAT, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_MA_BAO_MAT, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_KY_HOC, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_HANH_KIEM, "null");
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_LOP_BI_AN, "null");

        //Move Login screen and finish
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String lop = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_CLASS);
        currentPosSelected = lop.equals("null") ? 0 : Integer.parseInt(lop) - 1;
        if (lop.equals("null")) {
            DataUtils.getInstance().saveValue(MainActivity.this, Data.DATA, Data.KEY_CLASS, "1");
        }
        binding.navigation.getMenu().getItem(currentPosSelected).setChecked(true);

        setTitle(currentPosSelected);

        hideLopBiAn();
    }

    private void hideLopBiAn() {
        String viTriLopBiAn = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_LOP_BI_AN);
        if (viTriLopBiAn.equals("null") || viTriLopBiAn.equals("")) {
            for (int i = 0; i < 12; i++) {
                binding.navigation.getMenu().getItem(i).setVisible(true);
            }
            return;
        }

        StringTokenizer tokenizer = new StringTokenizer(viTriLopBiAn);
        int[] viTriBiAn = new int[tokenizer.countTokens()];
        for (int i = 0; i < viTriBiAn.length; i++) {
            viTriBiAn[i] = Integer.parseInt(tokenizer.nextToken());
        }
        for (int i = 0; i < 12; i++) {
            binding.navigation.getMenu().getItem(i).setVisible(true);
            for (int k : viTriBiAn) {
                if (i == k) {
                    binding.navigation.getMenu().getItem(i).setVisible(false);
                    break;
                }
            }
        }

    }

    private void initUserInfo() {
        String username = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_USERNAME);
        database.child(username).child(Data.ACCOUNT).child(Data.NAME).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String hoTen = snapshot.getValue().toString();
                navBinding.txtName.setText(hoTen.equals("null") ? "Name is Wrong" : hoTen);
                DataUtils.getInstance().saveValue(MainActivity.this, Data.DATA, Data.KEY_NAME, hoTen);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.class_1:
                setSelectedItem(0);
                break;
            case R.id.class_2:
                setSelectedItem(1);
                break;
            case R.id.class_3:
                setSelectedItem(2);
                break;
            case R.id.class_4:
                setSelectedItem(3);
                break;
            case R.id.class_5:
                setSelectedItem(4);
                break;
            case R.id.class_6:
                setSelectedItem(5);
                break;
            case R.id.class_7:
                setSelectedItem(6);
                break;
            case R.id.class_8:
                setSelectedItem(7);
                break;
            case R.id.class_9:
                setSelectedItem(8);
                break;
            case R.id.class_10:
                setSelectedItem(9);
                break;
            case R.id.class_11:
                setSelectedItem(10);
                break;
            case R.id.class_12:
                setSelectedItem(11);
                break;
            case R.id.cai_dat:
                startActivity(new Intent(this, CaiDatActivity.class));
                break;
            case R.id.game:
                startActivity(new Intent(this, CaroActivity.class));
                break;
            case R.id.bang_tuan_hoan:
                startActivity(new Intent(this, BangTuanHoanActivity.class));
                break;
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                binding.drawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 240);
        return true;
    }

    private void setSelectedItem(int pos) {
        DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_CLASS, String.valueOf(pos + 1));
        if (currentPosSelected != pos) {
            binding.navigation.getMenu().getItem(pos).setChecked(true);
            binding.navigation.getMenu().getItem(currentPosSelected).setChecked(false);
            currentPosSelected = pos;
            setTitle(pos);
            if (tongQuanListener != null) {
                tongQuanListener.changeClassListener();
            }
            if (bangDiemListener != null) {
                bangDiemListener.changeClassListener();
            }
            if (bieuDoListener != null) {
                bieuDoListener.changeClassListener();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressLint("SetTextI18n")
    public void setTitle(int pos) {
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        if (hocKy.equals(Data.VALUE_KY_1)) {
            hocKy = " - Kỳ 1";
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            hocKy = " - Kỳ 2";
        } else {
            hocKy = " - " + getString(R.string.tong_ket);
        }
        binding.txtTitle.setText(classList[pos] + hocKy);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    public void setIconSuKien(boolean hasSuKienHomNay) {
        if (hasSuKienHomNay) {
            Glide.with(this).load(R.drawable.ic_reminder_3).into(binding.imgSuKien);
        } else {
            binding.imgSuKien.setImageResource(R.drawable.ic_notifications);
        }

    }

    public void setChangeClassTongQuanListener(ChangeClassListener listener) {
        this.tongQuanListener = listener;
    }

    public void setChangeClassBangDiemListener(ChangeClassListener listener) {
        this.bangDiemListener = listener;
    }

    public void setChangeClassBieuDoListener(ChangeClassListener listener) {
        this.bieuDoListener = listener;
    }

    public void setChangeHocKyTongQuanListener(ChangeHocKyListener listener) {
        this.changeHocKyTongQuanListener = listener;
    }

    public void setChangeHocKyBangDiemListener(ChangeHocKyListener listener) {
        this.changeHocKyBangDiemListener = listener;
    }

    public void setChangeHocKyBieuDoListener(ChangeHocKyListener listener) {
        this.changeHocKyBieuDoListener = listener;
    }

}