package com.leenguyen.studentnote.screens.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.ItemBieuDoMonHocBinding;
import com.leenguyen.studentnote.screens.main.ItemBieuDoClickListener;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.screens.main.model.MonHocWithTbmModel;

import java.util.ArrayList;

public class BieuDoAdapter extends RecyclerView.Adapter<BieuDoAdapter.BieuDoViewHolder> {
    private ArrayList<MonHocWithTbmModel> list;
    private Context context;
    private ItemBieuDoClickListener listener;

    public BieuDoAdapter(Context context, ItemBieuDoClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<MonHocWithTbmModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BieuDoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBieuDoMonHocBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_bieu_do_mon_hoc, parent, false);
        return new BieuDoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BieuDoViewHolder holder, int position) {
        holder.binding.txtThuTu.setText(String.valueOf(position + 1));
        holder.binding.txtTenMonHoc.setText(list.get(position).getTenMonHoc());
        int diem = list.get(position).getTbm();//Thang diem 100
        //Chuyen ve thang diem 10
        double diem2 = diem;
        diem2 = diem2 / 10;
        holder.binding.txtDiem.setText(String.valueOf(diem2));
        holder.binding.cardItem.setOnClickListener(v -> listener.itemBieuDoClickListener(list.get(position).getId()));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class BieuDoViewHolder extends RecyclerView.ViewHolder {
        private ItemBieuDoMonHocBinding binding;

        public BieuDoViewHolder(ItemBieuDoMonHocBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
