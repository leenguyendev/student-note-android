package com.leenguyen.studentnote.screens.monhocdetail;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivityMonHocDetailBinding;
import com.leenguyen.studentnote.databinding.DialogChonHocKyBinding;
import com.leenguyen.studentnote.databinding.DialogEditMonHocBinding;
import com.leenguyen.studentnote.databinding.DialogThemDiemBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.dialogs.OkButtonClickListener;
import com.leenguyen.studentnote.model.DiemMonHocModel;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.model.TinhDiemHocKyModel;
import com.leenguyen.studentnote.screens.main.fragment.TongQuanFragment;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MonHocDetailActivity extends BaseActivity<ActivityMonHocDetailBinding> implements RemoveDiemListener, OkButtonClickListener {
    private static final int HE_SO_1 = 1;
    private static final int HE_SO_2 = 2;
    private static final int HE_SO_3 = 3;
    private String id;
    private String userName, lop;
    private MonHocModel monHocModel;
    private DiemAdapter diemHS1Adapter, diemHS2Adapter, diemHS3Adapter;
    private List<String> diemHS1, diemHS2, diemHS3;

    @Override
    protected void initLayout() {
        Intent intent = getIntent();
        id = intent.getStringExtra(TongQuanFragment.KEY_ID_MON_HOC);
        binding.btnBack.setOnClickListener(v -> finish());
        userName = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_USERNAME);
        lop = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_CLASS);
        binding.btnMenuHocKy.setOnClickListener(v -> showChonHocKyDialog());
        binding.btnMenuEdit.setOnClickListener(v -> showDialogEdit());
        binding.btnMenuDelete.setOnClickListener(v -> showDialogDelete());
        binding.imgFavorite.setOnClickListener(v -> {
            boolean favorite = monHocModel.getIs_mon_chuyen().equals(Data.VALUE_IS_MON_CHUYEN);
            binding.imgFavorite.setImageResource(favorite ? R.drawable.ic_not_favorite : R.drawable.ic_favorite);
            String favoriteValue = favorite ? Data.VALUE_IS_NOT_MON_CHUYEN : Data.VALUE_IS_MON_CHUYEN;
            database.child(userName).child(lop).child(Data.MON_HOC).child(id).child(Data.IS_MON_CHUYEN).setValue(favoriteValue);
        });
        getThongTinMonHoc(id);

        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        LinearLayoutManager manager2 = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        LinearLayoutManager manager3 = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);

        diemHS1Adapter = new DiemAdapter(this, HE_SO_1, this);
        binding.rcvDiemHeSo1.setAdapter(diemHS1Adapter);
        binding.rcvDiemHeSo1.setLayoutManager(manager);

        diemHS2Adapter = new DiemAdapter(this, HE_SO_2, this);
        binding.rcvDiemHeSo2.setAdapter(diemHS2Adapter);
        binding.rcvDiemHeSo2.setLayoutManager(manager2);

        diemHS3Adapter = new DiemAdapter(this, HE_SO_3, this);
        binding.rcvDiemHeSo3.setAdapter(diemHS3Adapter);
        binding.rcvDiemHeSo3.setLayoutManager(manager3);

        binding.btnAddDiemHs1.setOnClickListener(v -> showDialogThemDiem(HE_SO_1));
        binding.btnAddDiemHs2.setOnClickListener(v -> showDialogThemDiem(HE_SO_2));
        binding.btnAddDiemHs3.setOnClickListener(v -> showDialogThemDiem(HE_SO_3));

        binding.btnRemoveDiemHs1.setOnClickListener(v -> {
            diemHS1Adapter.setShowRemoveButton(!diemHS1Adapter.getShowRemoveButton());
            binding.btnRemoveDiemHs1.setImageResource(diemHS1Adapter.getShowRemoveButton() ? R.drawable.ic_not_remove : R.drawable.ic_remove);
        });
        binding.btnRemoveDiemHs2.setOnClickListener(v -> {
            diemHS2Adapter.setShowRemoveButton(!diemHS2Adapter.getShowRemoveButton());
            binding.btnRemoveDiemHs2.setImageResource(diemHS2Adapter.getShowRemoveButton() ? R.drawable.ic_not_remove : R.drawable.ic_remove);
        });
        binding.btnRemoveDiemHs3.setOnClickListener(v -> {
            diemHS3Adapter.setShowRemoveButton(!diemHS3Adapter.getShowRemoveButton());
            binding.btnRemoveDiemHs3.setImageResource(diemHS3Adapter.getShowRemoveButton() ? R.drawable.ic_not_remove : R.drawable.ic_remove);
        });

        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        binding.cardTbmThanhPhan.setVisibility(hocKy.equals(Data.VALUE_TONG_KET) ? View.VISIBLE : View.GONE);
        binding.loDiemThanhPhan.setVisibility(hocKy.equals(Data.VALUE_TONG_KET) ? View.GONE : View.VISIBLE);
    }

    private void showDialogDelete() {
        NoticeDialog.getInstance().showNoticeDialog(this, getString(R.string.xoa_mon_hoc),
                "Bạn có chắc chắn muốn xóa môn " + monHocModel.getTen_mon_hoc() + "?", this);
    }

    private void showDialogEdit() {
        Dialog dialog = new Dialog(this);
        DialogEditMonHocBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_edit_mon_hoc, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(false);
        String tenMonHoc = monHocModel.getTen_mon_hoc();
        String heSoMonHoc = monHocModel.getHe_so_mon();
        bindingDialog.edtTenMonHoc.setText(tenMonHoc);
        switch (heSoMonHoc) {
            case Data.VALUE_HE_SO_MON_0:
                bindingDialog.rdbHs0.setChecked(true);
                break;
            case Data.VALUE_HE_SO_MON_1:
                bindingDialog.rdbHs1.setChecked(true);
                break;
            case Data.VALUE_HE_SO_MON_2:
                bindingDialog.rdbHs2.setChecked(true);
                break;
            case Data.VALUE_HE_SO_MON_3:
                bindingDialog.rdbHs3.setChecked(true);
                break;
        }
        String mucTieu;
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        switch (hocKy) {
            case Data.VALUE_KY_1:
                mucTieu = monHocModel.getHoc_ky_1().getMuc_tieu();
                break;
            case Data.VALUE_KY_2:
                mucTieu = monHocModel.getHoc_ky_2().getMuc_tieu();
                break;
            default:
                mucTieu = monHocModel.getMuc_tieu();
                break;
        }
        bindingDialog.pkNumber.setValue(Integer.parseInt(mucTieu));
        bindingDialog.edtTenMonHoc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bindingDialog.txtError.setVisibility(s.length() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        bindingDialog.btnOk.setOnClickListener(v -> {
            if (!Utils.getInstance().isOnline(this)) {
                Toast.makeText(this, getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
                return;
            }
            if (bindingDialog.txtError.getVisibility() == View.GONE) {
                String tenMonHocNew = bindingDialog.edtTenMonHoc.getText().toString().trim();
                if (!tenMonHocNew.equals(tenMonHoc)) {
                    monHocModel.setTen_mon_hoc(tenMonHocNew);
                }
                String heSoMonNew;
                if (bindingDialog.rdbHs0.isChecked()) {
                    heSoMonNew = Data.VALUE_HE_SO_MON_0;
                } else if (bindingDialog.rdbHs1.isChecked()) {
                    heSoMonNew = Data.VALUE_HE_SO_MON_1;
                } else if (bindingDialog.rdbHs2.isChecked()) {
                    heSoMonNew = Data.VALUE_HE_SO_MON_2;
                } else {
                    heSoMonNew = Data.VALUE_HE_SO_MON_3;
                }
                if (!heSoMonHoc.equals(heSoMonNew)) {
                    monHocModel.setHe_so_mon(heSoMonNew);
                }
                switch (hocKy) {
                    case Data.VALUE_KY_1:
                        monHocModel.getHoc_ky_1().setMuc_tieu(String.valueOf(bindingDialog.pkNumber.getValue()));
                        break;
                    case Data.VALUE_KY_2:
                        monHocModel.getHoc_ky_2().setMuc_tieu(String.valueOf(bindingDialog.pkNumber.getValue()));
                        break;
                    default:
                        monHocModel.setMuc_tieu(String.valueOf(bindingDialog.pkNumber.getValue()));
                        break;
                }
                database.child(userName).child(lop).child(Data.MON_HOC).child(id).setValue(monHocModel);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void showDialogThemDiem(int heSo) {
        Dialog dialog = new Dialog(this);
        DialogThemDiemBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_them_diem, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(false);
        bindingDialog.txtTitle.setText(getString(R.string.them_diem_he_so) + " " + heSo);
        bindingDialog.imgClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.edtDiem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean isNumber = false;
                try {
                    double diemMoi = Double.parseDouble(s.toString());
                    if (diemMoi <= 10 && diemMoi >= 0 && !String.valueOf(s.charAt(0)).equals(".")) {
                        isNumber = true;
                    } else {
                        isNumber = false;
                    }
                } catch (Exception e) {
                    isNumber = false;
                }
                bindingDialog.txtDiemError.setVisibility(isNumber ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        bindingDialog.btnOk.setOnClickListener(v -> {
            if (!Utils.getInstance().isOnline(this)) {
                Toast.makeText(this, getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
                return;
            }
            if (bindingDialog.edtDiem.getText().length() == 0) {
                bindingDialog.txtDiemError.setVisibility(View.VISIBLE);
            }
            if (bindingDialog.txtDiemError.getVisibility() == View.GONE) {
                //Gui diem mới lên server
                String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
                DiemMonHocModel diemMonHocModel;
                switch (hocKy) {
                    case Data.VALUE_KY_1:
                        hocKy = Data.HOC_KY_1;
                        diemMonHocModel = monHocModel.getHoc_ky_1();
                        break;
                    case Data.VALUE_KY_2:
                        hocKy = Data.HOC_KY_2;
                        diemMonHocModel = monHocModel.getHoc_ky_2();
                        break;
                    default:
                        diemMonHocModel = monHocModel.getHoc_ky_2();
                        break;
                }
                if (heSo == HE_SO_1) {
                    String cacDiemHienTai = diemMonHocModel.getDiem_he_so_1();
                    database.child(userName).child(lop).child(Data.MON_HOC).child(id)
                            .child(hocKy).child(Data.DIEM_HE_SO_1).setValue((cacDiemHienTai + " " + bindingDialog.edtDiem.getText().toString()).trim());
                } else if (heSo == HE_SO_2) {
                    String cacDiemHienTai = diemMonHocModel.getDiem_he_so_2();
                    database.child(userName).child(lop).child(Data.MON_HOC).child(id)
                            .child(hocKy).child(Data.DIEM_HE_SO_2).setValue((cacDiemHienTai + " " + bindingDialog.edtDiem.getText().toString()).trim());
                } else {
                    String cacDiemHienTai = diemMonHocModel.getDiem_he_so_3();
                    database.child(userName).child(lop).child(Data.MON_HOC).child(id)
                            .child(hocKy).child(Data.DIEM_HE_SO_3).setValue((cacDiemHienTai + " " + bindingDialog.edtDiem.getText().toString()).trim());
                }
                Toast.makeText(this, getString(R.string.them_diem_thanh_cong), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showChonHocKyDialog() {
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        Dialog dialog = new Dialog(this);
        DialogChonHocKyBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_chon_hoc_ky, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        dialog.setCancelable(true);
        switch (hocKy) {
            case "null":
            case Data.VALUE_TONG_KET:
                bindingDialog.rdbTongKet.setChecked(true);
                break;
            case Data.VALUE_KY_1:
                bindingDialog.rdbKy1.setChecked(true);
                break;
            case Data.VALUE_KY_2:
                bindingDialog.rdbKy2.setChecked(true);
                break;
        }

        bindingDialog.btnOk.setOnClickListener(v -> {
            String hockyFinal;
            if (bindingDialog.rdbKy1.isChecked()) {
                hockyFinal = Data.VALUE_KY_1;
            } else if (bindingDialog.rdbKy2.isChecked()) {
                hockyFinal = Data.VALUE_KY_2;
            } else {
                hockyFinal = Data.VALUE_TONG_KET;
            }
            DataUtils.getInstance().saveValue(this, Data.DATA, Data.KEY_KY_HOC, hockyFinal);
            showData();
            showDiemHS1();
            showDiemHS2();
            showDiemHS3();
            dialog.dismiss();
        });
        dialog.show();
    }

    private void getThongTinMonHoc(String id) {
        database.child(userName).child(lop).child(Data.MON_HOC).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    monHocModel = snapshot.getValue(MonHocModel.class);
                    showData();
                    showDiemHS1();
                    showDiemHS2();
                    showDiemHS3();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showDiemHS3() {
        if (diemHS3 == null) {
            diemHS3 = new ArrayList<>();
        }
        diemHS3.clear();
        StringTokenizer tokenizer = null;
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        if (hocKy.equals(Data.VALUE_KY_1)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_1().getDiem_he_so_3());
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_2().getDiem_he_so_3());
        } else {
            diemHS3.clear();
        }
        if (tokenizer != null) {
            while (tokenizer.countTokens() > 0) {
                diemHS3.add(tokenizer.nextToken());
            }
        }
        diemHS3Adapter.setData(diemHS3);
    }

    private void showDiemHS2() {
        if (diemHS2 == null) {
            diemHS2 = new ArrayList<>();
        }
        diemHS2.clear();
        StringTokenizer tokenizer = null;
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        if (hocKy.equals(Data.VALUE_KY_1)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_1().getDiem_he_so_2());
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_2().getDiem_he_so_2());
        } else {
            diemHS2.clear();
        }
        if (tokenizer != null) {
            while (tokenizer.countTokens() > 0) {
                diemHS2.add(tokenizer.nextToken());
            }
        }
        diemHS2Adapter.setData(diemHS2);
    }

    private void showDiemHS1() {
        if (diemHS1 == null) {
            diemHS1 = new ArrayList<>();
        }
        diemHS1.clear();
        StringTokenizer tokenizer = null;
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        if (hocKy.equals(Data.VALUE_KY_1)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_1().getDiem_he_so_1());
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            tokenizer = new StringTokenizer(monHocModel.getHoc_ky_2().getDiem_he_so_1());
        } else {
            diemHS1.clear();
        }
        if (tokenizer != null) {
            while (tokenizer.countTokens() > 0) {
                diemHS1.add(tokenizer.nextToken());
            }
        }
        diemHS1Adapter.setData(diemHS1);
    }

    @SuppressLint("SetTextI18n")
    private void showData() {
        binding.txtTenMonHoc.setText(monHocModel.getTen_mon_hoc());
        binding.txtHeSoMonHoc.setText(String.valueOf(Integer.parseInt(monHocModel.getHe_so_mon())));
        binding.imgFavorite.setImageResource(monHocModel.getIs_mon_chuyen().equals(Data.VALUE_IS_MON_CHUYEN) ? R.drawable.ic_favorite : R.drawable.ic_not_favorite);
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        int mucTieu;
        TinhDiemHocKyModel diemHocKyModel;
        if (hocKy.equals(Data.VALUE_KY_1)) {
            binding.txtTitleHocKy.setText("Lớp " + lop + " - " + getString(R.string.ky_1));
            mucTieu = Integer.parseInt(monHocModel.getHoc_ky_1().getMuc_tieu());
            diemHocKyModel = Utils.getInstance().tinhDiemHocKy(monHocModel.getHoc_ky_1().getDiem_he_so_1(),
                    monHocModel.getHoc_ky_1().getDiem_he_so_2(),
                    monHocModel.getHoc_ky_1().getDiem_he_so_3());
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            binding.txtTitleHocKy.setText("Lớp " + lop + " - " + getString(R.string.ky_2));
            mucTieu = Integer.parseInt(monHocModel.getHoc_ky_2().getMuc_tieu());
            diemHocKyModel = Utils.getInstance().tinhDiemHocKy(monHocModel.getHoc_ky_2().getDiem_he_so_1(),
                    monHocModel.getHoc_ky_2().getDiem_he_so_2(),
                    monHocModel.getHoc_ky_2().getDiem_he_so_3());
        } else {
            binding.txtTitleHocKy.setText("Lớp " + lop + " - " + getString(R.string.tong_ket));
            mucTieu = Integer.parseInt(monHocModel.getMuc_tieu());
            TinhDiemHocKyModel diemHocKyModel1 = Utils.getInstance().tinhDiemHocKy(
                    monHocModel.getHoc_ky_1().getDiem_he_so_1(),
                    monHocModel.getHoc_ky_1().getDiem_he_so_2(),
                    monHocModel.getHoc_ky_1().getDiem_he_so_3());
            int mauSoKy1 = diemHocKyModel1.getMauSo();
            double trungBinhKy1 = diemHocKyModel1.getDiemTrungBinh();
            binding.txtTbmKy1.setText(mauSoKy1 > 0 ? String.valueOf((double) Math.round(trungBinhKy1 * 10) / 10) : "-:-");
            TinhDiemHocKyModel diemHocKyModel2 = Utils.getInstance().tinhDiemHocKy(
                    monHocModel.getHoc_ky_2().getDiem_he_so_1(),
                    monHocModel.getHoc_ky_2().getDiem_he_so_2(),
                    monHocModel.getHoc_ky_2().getDiem_he_so_3());
            int mauSoKy2 = diemHocKyModel2.getMauSo();
            double trungBinhKy2 = diemHocKyModel2.getDiemTrungBinh();
            binding.txtTbmKy2.setText(mauSoKy2 > 0 ? String.valueOf((double) Math.round(trungBinhKy2 * 10) / 10) : "-:-");
            double trungBinhCaNam = 0;
            if (mauSoKy1 > 0 && mauSoKy2 > 0) {
                trungBinhCaNam = (trungBinhKy1 + 2 * trungBinhKy2) / 3;
            } else if (mauSoKy1 == 0) {
                trungBinhCaNam = trungBinhKy2;
            } else if (mauSoKy2 == 0) {
                trungBinhCaNam = trungBinhKy1;
            }
            diemHocKyModel = new TinhDiemHocKyModel(trungBinhCaNam, mauSoKy1 + mauSoKy2);
        }
        binding.cardTbmThanhPhan.setVisibility(hocKy.equals(Data.VALUE_TONG_KET) ? View.VISIBLE : View.GONE);
        binding.loDiemThanhPhan.setVisibility(hocKy.equals(Data.VALUE_TONG_KET) ? View.GONE : View.VISIBLE);
        binding.mucTieu.setProgress(mucTieu);
        binding.txtTitleMucTieu.setText(getString(R.string.muc_tieu) + " (" + mucTieu + "/100)");
        binding.diemHienTai.setProgress((int) (10 * diemHocKyModel.getDiemTrungBinh()));
        binding.txtTitleDiemHienTai.setText(getString(R.string.hien_tai) + " (" + (int) (double) Math.round(diemHocKyModel.getDiemTrungBinh() * 10) + "/100)");
        binding.txtTbm.setText(diemHocKyModel.getMauSo() > 0 ? String.valueOf((double) Math.round(diemHocKyModel.getDiemTrungBinh() * 10) / 10) : "-:-");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_mon_hoc_detail;
    }

    @Override
    public void removeDiemListener(int type, int pos) {
        if (!Utils.getInstance().isOnline(this)) {
            Toast.makeText(this, getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
            return;
        }
        String hocKy = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_KY_HOC);
        hocKy = hocKy.equals(Data.VALUE_KY_1) ? Data.HOC_KY_1 : Data.HOC_KY_2;
        StringBuilder listDiemNew = new StringBuilder();
        if (type == HE_SO_1) {
            diemHS1.remove(pos);
            for (String diem : diemHS1) {
                listDiemNew.append(diem).append(" ");
            }
            database.child(userName).child(lop).child(Data.MON_HOC).child(id).child(hocKy).child(Data.DIEM_HE_SO_1).setValue(listDiemNew.toString().trim());
        } else if (type == HE_SO_2) {
            diemHS2.remove(pos);
            for (String diem : diemHS2) {
                listDiemNew.append(diem).append(" ");
            }
            database.child(userName).child(lop).child(Data.MON_HOC).child(id).child(hocKy).child(Data.DIEM_HE_SO_2).setValue(listDiemNew.toString().trim());
        } else {
            diemHS3.remove(pos);
            for (String diem : diemHS3) {
                listDiemNew.append(diem).append(" ");
            }
            database.child(userName).child(lop).child(Data.MON_HOC).child(id).child(hocKy).child(Data.DIEM_HE_SO_3).setValue(listDiemNew.toString().trim());
        }
    }

    @Override
    public void okButtonClickListener() {
        if (Utils.getInstance().isOnline(this)) {
            database.child(userName).child(lop).child(Data.MON_HOC).child(id).setValue(null);
            Toast.makeText(this, getString(R.string.mon_hoc_da_duoc_xoa), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
        }

    }
}