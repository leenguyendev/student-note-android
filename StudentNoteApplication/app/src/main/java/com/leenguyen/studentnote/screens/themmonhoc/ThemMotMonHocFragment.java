package com.leenguyen.studentnote.screens.themmonhoc;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentThemMotMonHocBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.model.DiemMonHocModel;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

public class ThemMotMonHocFragment extends BaseFragment<FragmentThemMotMonHocBinding> implements SaveClickListener {
    private String username, lop;

    @Override
    protected void initLayout() {
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        ((ThemMonHocActivity) getActivity()).setSaveListener(this);
        binding.rdbHs1.setChecked(true);
        binding.rdbHs0.setOnCheckedChangeListener((buttonView, isChecked) -> binding.loMucTieu.setVisibility(isChecked ? View.GONE : View.VISIBLE));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_them_mot_mon_hoc;
    }

    @Override
    public void saveMotMonListener() {
        if (Utils.getInstance().isOnline(getContext()) && binding.edtTenMonHoc.getText().length() > 0) {
            String tenMonHoc = binding.edtTenMonHoc.getText().toString();
            String mucTieu = String.valueOf(binding.pkNumber.getValue());
            String heSoMonHoc;
            if (binding.rdbHs0.isChecked()) {
                heSoMonHoc = "0";
            } else if (binding.rdbHs1.isChecked()) {
                heSoMonHoc = "1";
            } else if (binding.rdbHs2.isChecked()) {
                heSoMonHoc = "2";
            } else {
                heSoMonHoc = "3";
            }
            DiemMonHocModel hocKy1 = new DiemMonHocModel("", "", "", mucTieu);
            DiemMonHocModel hocKy2 = new DiemMonHocModel("", "", "", mucTieu);
            MonHocModel monHocModel = new MonHocModel(tenMonHoc, mucTieu, heSoMonHoc, "0", hocKy1, hocKy2);
            database.child(username).child(lop).child(Data.MON_HOC).child(Utils.getInstance().getCurrentTime()).setValue(monHocModel);
            Toast.makeText(getContext(), "Thêm môn học thành công", Toast.LENGTH_LONG).show();
            binding.edtTenMonHoc.setText("");
        } else if (!Utils.getInstance().isOnline(getContext())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.can_co_ket_noi_mang), null);
        } else if (!(binding.edtTenMonHoc.getText().length() > 0)) {
            Toast.makeText(getContext(), "Bạn chưa nhập tên môn học", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void saveDanhSachListener() {
        //Not implement
    }
}
