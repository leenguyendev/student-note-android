package com.leenguyen.studentnote.constants;

public class Data {
    //SharePreference
    public static final String DATA = "com.leenguyen.studentnote.constants.data";
    public static final String KEY_CLASS = "com.leenguyen.studentnote.constants.key_class";
    public static final String KEY_NAME = "com.leenguyen.studentnote.constants.key_name";
    public static final String KEY_USERNAME = "com.leenguyen.studentnote.constants.key_username";
    public static final String KEY_PASSWORD = "com.leenguyen.studentnote.constants.password";
    public static final String KEY_AVATAR = "com.leenguyen.studentnote.constants.avatar";
    public static final String KEY_AVATAR_ROTATE_ANGLE = "com.leenguyen.studentnote.constants.avatar_angle";
    public static final String KEY_MA_BAO_MAT = "com.leenguyen.studentnote.constants.ma_bao_mat";
    public static final String KEY_LOP_BI_AN = "com.leenguyen.studentnote.constants.lop_bi_an";

    public static final String KEY_IS_USE_MA_BAO_MAT = "com.leenguyen.studentnote.constants.use_ma_bao_mat";
    public static final String VALUE_IS_USE_MA_BAO_MAT = "1";
    public static final String VALUE_IS_NOT_USE_MA_BAO_MAT = "0";

    public static final String KEY_IS_LOGIN = "com.leenguyen.studentnote.constants.key_is_login";
    public static final String VALUE_IS_LOGIN = "1";
    public static final String VALUE_IS_NOT_LOGIN = "0";

    public static final String KEY_KY_HOC = "com.leenguyen.studentnote.constants.key_ky_hoc";
    public static final String VALUE_KY_1 = "1";
    public static final String VALUE_KY_2 = "2";
    public static final String VALUE_TONG_KET = "3";

    public static final String KEY_HANH_KIEM = "com.leenguyen.studentnote.constants.key_hanh_kiem";
    public static final String VALUE_TOT = "1";
    public static final String VALUE_KHA = "2";
    public static final String VALUE_TRUNG_BINH = "3";
    public static final String VALUE_YEU = "4";

    public static final String VALUE_IS_MON_CHUYEN = "1";
    public static final String VALUE_IS_NOT_MON_CHUYEN = "0";

    public static final String VALUE_HE_SO_MON_0 = "0";
    public static final String VALUE_HE_SO_MON_1 = "1";
    public static final String VALUE_HE_SO_MON_2 = "2";
    public static final String VALUE_HE_SO_MON_3 = "3";


    //Firebase Database
    public static final String ACCOUNT = "account";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String MON_HOC = "mon_hoc";
    public static final String DIEM_HE_SO_1 = "diem_he_so_1";
    public static final String DIEM_HE_SO_2 = "diem_he_so_2";
    public static final String DIEM_HE_SO_3 = "diem_he_so_3";
    public static final String IS_MON_CHUYEN = "is_mon_chuyen";
    public static final String HANH_KIEM = "hanh_kiem";
    public static final String HOC_KY_1 = "hoc_ky_1";
    public static final String HOC_KY_2 = "hoc_ky_2";
    public static final String TONG_KET = "tong_ket";
    public static final String LICH_HOC = "lich_hoc";
    public static final String SU_KIEN = "su_kien";
}
