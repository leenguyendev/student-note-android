package com.leenguyen.studentnote.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.util.Log;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.model.TinhDiemHocKyModel;
import com.leenguyen.studentnote.screens.main.model.MonHocWithTbmModel;
import com.leenguyen.studentnote.screens.sukien.model.SuKienModel;
import com.leenguyen.studentnote.screens.sukien.model.SuKienWithIdModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Utils {
    private static Utils instance;

    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    public String getCurrentTime() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public TinhDiemHocKyModel tinhDiemHocKy(String diemHS1, String diemHS2, String diemHS3) {
        double tong = 0, trungBinh = 0;
        StringTokenizer tokenizerDiemHs1 = new StringTokenizer(diemHS1);
        StringTokenizer tokenizerDiemHs2 = new StringTokenizer(diemHS2);
        StringTokenizer tokenizerDiemHs3 = new StringTokenizer(diemHS3);
        int mauSo = tokenizerDiemHs1.countTokens() + 2 * tokenizerDiemHs2.countTokens() + 3 * tokenizerDiemHs3.countTokens();
        while (tokenizerDiemHs1.countTokens() > 0) {
            tong = tong + Double.parseDouble(tokenizerDiemHs1.nextToken());
        }
        while (tokenizerDiemHs2.countTokens() > 0) {
            tong = tong + 2 * Double.parseDouble(tokenizerDiemHs2.nextToken());
        }
        while (tokenizerDiemHs3.countTokens() > 0) {
            tong = tong + 3 * Double.parseDouble(tokenizerDiemHs3.nextToken());
        }
        if (mauSo > 0) {
            trungBinh = tong / mauSo;
        }
        return new TinhDiemHocKyModel(trungBinh, mauSo);
    }

    public double tinhDiemTrungBinhMon(Context context, List<MonHocWithIDModel> list) {
        String hocKy = DataUtils.getInstance().getValue(context, Data.DATA, Data.KEY_KY_HOC);
        double trungBinhMon = 0, tong = 0;
        int mauSo = 0;

        if (hocKy.equals(Data.VALUE_KY_1)) {
            for (int i = 0; i < list.size(); i++) {
                MonHocWithIDModel monHocWithIDModel = list.get(i);
                int heSoMon = Integer.parseInt(monHocWithIDModel.getMonHoc().getHe_so_mon());

                double trungBinhKy1 = 0;

                String diemHeSo1HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_1();
                String diemHeSo2HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_2();
                String diemHeSo3HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_3();

                TinhDiemHocKyModel diemHocKy1Model = tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
                trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();

                tong = tong + trungBinhKy1 * heSoMon;
                if (trungBinhKy1 > 0) {
                    mauSo = mauSo + heSoMon;
                }
            }
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            for (int i = 0; i < list.size(); i++) {
                MonHocWithIDModel monHocWithIDModel = list.get(i);
                int heSoMon = Integer.parseInt(monHocWithIDModel.getMonHoc().getHe_so_mon());

                double trungBinhKy2 = 0;

                String diemHeSo1HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_1();
                String diemHeSo2HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_2();
                String diemHeSo3HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_3();

                TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
                trungBinhKy2 = diemHocKy1Model.getDiemTrungBinh();

                tong = tong + trungBinhKy2 * heSoMon;
                if (trungBinhKy2 > 0) {
                    mauSo = mauSo + heSoMon;
                }
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                MonHocWithIDModel monHocWithIDModel = list.get(i);
                int heSoMon = Integer.parseInt(monHocWithIDModel.getMonHoc().getHe_so_mon());

                double trungBinhKy1 = 0, trungBinhKy2 = 0, trungBinhCaNam = 0;

                String diemHeSo1HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_1();
                String diemHeSo2HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_2();
                String diemHeSo3HocKy1 = monHocWithIDModel.getMonHoc().getHoc_ky_1().getDiem_he_so_3();

                TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
                int mauSoKy1 = diemHocKy1Model.getMauSo();
                trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();

                String diemHeSo1HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_1();
                String diemHeSo2HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_2();
                String diemHeSo3HocKy2 = monHocWithIDModel.getMonHoc().getHoc_ky_2().getDiem_he_so_3();
                TinhDiemHocKyModel diemHocKy2Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
                int mauSoKy2 = diemHocKy2Model.getMauSo();
                trungBinhKy2 = diemHocKy2Model.getDiemTrungBinh();

                if (mauSoKy1 > 0 && mauSoKy2 > 0) {
                    trungBinhCaNam = (trungBinhKy1 + 2 * trungBinhKy2) / 3;
                } else if (mauSoKy1 == 0) {
                    trungBinhCaNam = trungBinhKy2;
                } else if (mauSoKy2 == 0) {
                    trungBinhCaNam = trungBinhKy1;
                }

                tong = tong + trungBinhCaNam * heSoMon;
                if (trungBinhCaNam > 0) {
                    mauSo = mauSo + heSoMon;
                }
            }
        }
        trungBinhMon = (double) Math.round(tong / mauSo * 10) / 10;
        return trungBinhMon;
    }

    public static ArrayList<MonHocWithTbmModel> sortMonHocByTbm (ArrayList<MonHocWithTbmModel> arrayList) {
        Collections.sort(arrayList, (lhs, rhs) -> {
            int lid = 0, rid = 0;
            lid = lhs.getTbm();
            rid = rhs.getTbm();
            if (lid > rid) {
                return -1;
            } else {
                return 1;
            }
        });
        return arrayList;
    }

    public static ArrayList<SuKienWithIdModel> sortSuKienByTime (ArrayList<SuKienWithIdModel> arrayList) {
        Collections.sort(arrayList, (lhs, rhs) -> {
            int lid = 0, rid = 0;
            lid = Integer.parseInt(lhs.getSuKienModel().getThoiGian());
            rid = Integer.parseInt(rhs.getSuKienModel().getThoiGian());
            if (lid > rid) {
                return -1;
            } else {
                return 1;
            }
        });
        return arrayList;
    }

    public String getCurrentVersion(Context context) {
        String version = "0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

}
