package com.leenguyen.studentnote.screens.nhapmabaomat;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;

import com.bumptech.glide.Glide;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ActivityNhapMaBaoMatBinding;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.utils.DataUtils;

public class NhapMaBaoMatActivity extends BaseActivity<ActivityNhapMaBaoMatBinding> {

    @Override
    protected void initLayout() {
        Glide.with(this).load(R.drawable.ic_ami_luom).into(binding.imgSticker);
        String maBaoMat = DataUtils.getInstance().getValue(this, Data.DATA, Data.KEY_MA_BAO_MAT);
        binding.soThu1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu2.requestFocus();
                }
            }
        });

        binding.soThu2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu3.requestFocus();
                } else {
                    binding.soThu1.requestFocus();
                }
            }
        });

        binding.soThu3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.soThu4.requestFocus();
                } else {
                    binding.soThu2.requestFocus();
                }
            }
        });

        binding.soThu4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    binding.soThu3.requestFocus();
                } else {
                    String so1 = binding.soThu1.getText().toString();
                    String so2 = binding.soThu2.getText().toString();
                    String so3 = binding.soThu3.getText().toString();
                    String so4 = binding.soThu4.getText().toString();
                    String maBaoMatXacMinh = so1 + so2 + so3 + so4;
                    if (maBaoMatXacMinh.equals(maBaoMat)) {
                        startActivity(new Intent(NhapMaBaoMatActivity.this, MainActivity.class));
                    }
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_nhap_ma_bao_mat;
    }
}