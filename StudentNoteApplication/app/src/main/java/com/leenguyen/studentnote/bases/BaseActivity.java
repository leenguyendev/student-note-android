package com.leenguyen.studentnote.bases;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.utils.Utils;

import org.jetbrains.annotations.Nullable;

public abstract class BaseActivity<B extends ViewDataBinding> extends AppCompatActivity {
    protected B binding;
    protected DatabaseReference database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = DataBindingUtil.setContentView(this, getLayoutResourceId());
        database = FirebaseDatabase.getInstance("https://student-note-hoc-sinh.firebaseio.com/").getReference();
        initLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.getInstance().isOnline(this)) {
            Toast.makeText(this, getString(R.string.khong_co_mang), Toast.LENGTH_SHORT).show();
        }
    }

    protected abstract void initLayout();

    protected abstract int getLayoutResourceId();
}
