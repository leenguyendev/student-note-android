package com.leenguyen.studentnote.screens.themmonhoc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationBarView;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.databinding.ActivityThemMonHocBinding;
import com.leenguyen.studentnote.screens.themmonhoc.adapter.ThemMonHocViewPagerAdapter;

public class ThemMonHocActivity extends BaseActivity<ActivityThemMonHocBinding> {
    private static final int THEM_1_POS = 0;
    private static final int THEM_DANH_SACH_POS = 1;
    private ThemMonHocViewPagerAdapter adapter;
    private SaveClickListener listener;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void initLayout() {
        adapter = new ThemMonHocViewPagerAdapter(this);
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                binding.bottomBar.getMenu().getItem(position).setChecked(true);
            }
        });
        binding.bottomBar.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.item_them_mot_mon:
                    binding.viewPager.setCurrentItem(0);
                    break;
                case R.id.item_them_nhieu:
                    binding.viewPager.setCurrentItem(1);
                    break;
            }
            return true;
        });
        binding.btnBack.setOnClickListener(v -> finish());
        binding.btnSave.setOnClickListener(v -> {
            int pos = binding.viewPager.getCurrentItem();
            if (pos == THEM_1_POS) {
                listener.saveMotMonListener();
            } else {
                listener.saveDanhSachListener();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_them_mon_hoc;
    }

    public void setSaveListener(SaveClickListener listener) {
        this.listener = listener;
    }
}