package com.leenguyen.studentnote.screens.main.fragment;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentBangDiemBinding;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.screens.main.ChangeClassListener;
import com.leenguyen.studentnote.screens.main.ChangeHocKyListener;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.screens.main.MainActivity;
import com.leenguyen.studentnote.screens.main.adapter.BangDiemAdapter;
import com.leenguyen.studentnote.screens.monhocdetail.MonHocDetailActivity;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class BangDiemFragment extends BaseFragment<FragmentBangDiemBinding> implements ChangeClassListener, ChangeHocKyListener, ItemMonHocClickListener {
    private BangDiemAdapter adapter;
    private List<MonHocWithIDModel> list;
    private String userName;

    @Override
    protected void initLayout() {
        ((MainActivity) getActivity()).setChangeClassBangDiemListener(this);
        ((MainActivity) getActivity()).setChangeHocKyBangDiemListener(this);
        userName = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        adapter = new BangDiemAdapter(getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        binding.rcvBangDiem.setLayoutManager(manager);
        binding.rcvBangDiem.setAdapter(adapter);
        adapter.setListener(this);
        getDanhSachMonHoc();
    }

    private void getDanhSachMonHoc() {
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        database.child(userName).child(lop).child(Data.MON_HOC).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String idMonHoc = dataSnapshot.getKey();
                    MonHocModel monHocModel = dataSnapshot.getValue(MonHocModel.class);
                    MonHocWithIDModel monHocWithIDModel = new MonHocWithIDModel(idMonHoc, monHocModel);
                    list.add(monHocWithIDModel);
                }
                showData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showData() {
        adapter.setData(list);
        binding.txtDiemTongKet.setText(String.valueOf(Utils.getInstance().tinhDiemTrungBinhMon(getContext(), list)));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_bang_diem;
    }

    @Override
    public void changeClassListener() {
        getDanhSachMonHoc();
    }

    @Override
    public void changeHocKyListener() {
        showData();
    }

    @Override
    public void itemClickListener(int position) {
        Intent intent = new Intent(getContext(), MonHocDetailActivity.class);
        intent.putExtra(TongQuanFragment.KEY_ID_MON_HOC, list.get(position).getId());
        startActivity(intent);
    }
}
