package com.leenguyen.studentnote.screens.sukien.model;

public class SuKienModel {
    private String thoiGian, tenSuKien, buoi, tiet, diaDiem;

    public SuKienModel(String thoiGian, String tenSuKien, String buoi, String tiet, String diaDiem) {
        this.thoiGian = thoiGian;
        this.tenSuKien = tenSuKien;
        this.buoi = buoi;
        this.tiet = tiet;
        this.diaDiem = diaDiem;
    }

    public SuKienModel() {
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public String getTenSuKien() {
        return tenSuKien;
    }

    public void setTenSuKien(String tenSuKien) {
        this.tenSuKien = tenSuKien;
    }

    public String getBuoi() {
        return buoi;
    }

    public void setBuoi(String buoi) {
        this.buoi = buoi;
    }

    public String getTiet() {
        return tiet;
    }

    public void setTiet(String tiet) {
        this.tiet = tiet;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    @Override
    public String toString() {
        return "{" +
                "thoiGian:'" + thoiGian + '\'' +
                ", tenSuKien:'" + tenSuKien + '\'' +
                ", buoi:'" + buoi + '\'' +
                ", tiet:'" + tiet + '\'' +
                ", diaDiem:'" + diaDiem + '\'' +
                '}';
    }
}
