package com.leenguyen.studentnote.screens.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ItemTongQuanBinding;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.model.TinhDiemHocKyModel;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

public class TongQuanAdapter extends RecyclerView.Adapter<TongQuanAdapter.TongQuanViewHolder> {
    private Context context;
    private List<MonHocWithIDModel> list;
    private String hocKy;
    private ItemMonHocClickListener listener;

    public TongQuanAdapter(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<MonHocWithIDModel> list) {
        hocKy = DataUtils.getInstance().getValue(context, Data.DATA, Data.KEY_KY_HOC);
        this.list = list;
        notifyDataSetChanged();
    }

    public void setListener(ItemMonHocClickListener listener) {
        this.listener = listener;
    }

    public static class TongQuanViewHolder extends RecyclerView.ViewHolder {
        private final ItemTongQuanBinding binding;

        public TongQuanViewHolder(ItemTongQuanBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @NonNull
    @Override
    public TongQuanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTongQuanBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_tong_quan, parent, false);
        return new TongQuanViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull TongQuanViewHolder holder, int position) {
        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        holder.binding.txtTenMonHoc.setText(list.get(position).getMonHoc().getTen_mon_hoc());
        holder.binding.txtTenMonHoc.setTextColor(color);

        String mucTieu, diemHienTai;
        int mauSo;
        if (hocKy.equals(Data.VALUE_KY_1)) {
            mucTieu = list.get(position).getMonHoc().getHoc_ky_1().getMuc_tieu();

            double trungBinhKy1 = 0;
            String diemHeSo1HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_1();
            String diemHeSo2HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_2();
            String diemHeSo3HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_3();
            TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
            trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();
            mauSo = diemHocKy1Model.getMauSo();
            diemHienTai = String.valueOf((double) Math.round(trungBinhKy1 * 10) / 10);
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            mucTieu = list.get(position).getMonHoc().getHoc_ky_2().getMuc_tieu();
            double trungBinhKy2 = 0;
            String diemHeSo1HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_1();
            String diemHeSo2HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_2();
            String diemHeSo3HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_3();
            TinhDiemHocKyModel diemHocKy2Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
            trungBinhKy2 = diemHocKy2Model.getDiemTrungBinh();
            mauSo = diemHocKy2Model.getMauSo();
            diemHienTai = String.valueOf((double) Math.round(trungBinhKy2 * 10) / 10);
        } else {
            double trungBinhKy1 = 0, trungBinhKy2 = 0, trungBinhCaNam = 0;

            mucTieu = list.get(position).getMonHoc().getMuc_tieu();

            String diemHeSo1HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_1();
            String diemHeSo2HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_2();
            String diemHeSo3HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_3();

            TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
            int mauSoKy1 = diemHocKy1Model.getMauSo();
            trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();

            String diemHeSo1HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_1();
            String diemHeSo2HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_2();
            String diemHeSo3HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_3();
            TinhDiemHocKyModel diemHocKy2Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
            int mauSoKy2 = diemHocKy2Model.getMauSo();
            trungBinhKy2 = diemHocKy2Model.getDiemTrungBinh();

            if (mauSoKy1 > 0 && mauSoKy2 > 0) {
                trungBinhCaNam = (trungBinhKy1 + 2 * trungBinhKy2) / 3;
            } else if (mauSoKy1 == 0) {
                trungBinhCaNam = trungBinhKy2;
            } else if (mauSoKy2 == 0) {
                trungBinhCaNam = trungBinhKy1;
            }
            mauSo = mauSoKy1 + mauSoKy2;
            diemHienTai = String.valueOf((double) Math.round(trungBinhCaNam * 10) / 10);
        }
        if (mauSo == 0) {
            holder.binding.txtDiem.setText("< -.- >");
        } else {
            holder.binding.txtDiem.setText(diemHienTai);
        }
        holder.binding.diemThucTe.setProgress((int) (10 * Double.parseDouble(diemHienTai)));
        holder.binding.diemMucTieu.setProgress(Integer.parseInt(mucTieu.equals("") ? "0" : mucTieu));
        holder.binding.imgMonChuyen.setVisibility(list.get(position).getMonHoc().getIs_mon_chuyen().equals("1") ? View.VISIBLE : View.GONE);
        holder.binding.itemMonHoc.setOnClickListener(v -> listener.itemClickListener(position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }
}
