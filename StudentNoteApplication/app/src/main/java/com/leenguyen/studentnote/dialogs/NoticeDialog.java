package com.leenguyen.studentnote.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;

import androidx.databinding.DataBindingUtil;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.DialogNoticeBinding;
import com.leenguyen.studentnote.screens.signup.SignUpActivity;

public class NoticeDialog {
    private static NoticeDialog instance;
    private Dialog dialog;
    private OkButtonClickListener listener;

    public static NoticeDialog getInstance() {
        if (instance == null) {
            instance = new NoticeDialog();
        }
        return instance;
    }

    public void showNoticeDialog(Context context, String title, String desc, OkButtonClickListener listener) {
        if (dialog != null) {
            dialog = null;
        }
        this.listener = listener;
        dialog = new Dialog(context);
        DialogNoticeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_notice, null, false);
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
        binding.txtTitle.setText(title);
        binding.txtDesc.setText(desc);
        binding.btnOk.setOnClickListener(v -> {
            dialog.dismiss();
            if (listener != null) {
                listener.okButtonClickListener();
            }
        });
        binding.imgClose.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }
}
