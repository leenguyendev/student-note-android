package com.leenguyen.studentnote.screens.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ItemBangDiemBinding;
import com.leenguyen.studentnote.model.MonHocWithIDModel;
import com.leenguyen.studentnote.model.TinhDiemHocKyModel;
import com.leenguyen.studentnote.screens.main.ItemMonHocClickListener;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.List;

public class BangDiemAdapter extends RecyclerView.Adapter<BangDiemAdapter.BangDiemViewHolder> {
    private Context context;
    private List<MonHocWithIDModel> list;
    private String hocKy;
    private ItemMonHocClickListener listener;

    public BangDiemAdapter(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<MonHocWithIDModel> list) {
        hocKy = DataUtils.getInstance().getValue(context, Data.DATA, Data.KEY_KY_HOC);
        this.list = list;
        notifyDataSetChanged();
    }

    public void setListener(ItemMonHocClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public BangDiemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBangDiemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_bang_diem, parent, false);
        return new BangDiemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BangDiemViewHolder holder, int position) {
        holder.binding.txtTenMonHoc.setText(list.get(position).getMonHoc().getTen_mon_hoc());
        holder.binding.cardItemMonHoc.setOnClickListener(v -> listener.itemClickListener(position));
        String diemHienTai;
        int mauSo;
        if (hocKy.equals(Data.VALUE_KY_1)) {
            double trungBinhKy1 = 0;
            String diemHeSo1HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_1();
            String diemHeSo2HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_2();
            String diemHeSo3HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_3();
            holder.binding.txtDiemHs1.setText(diemHeSo1HocKy1);
            holder.binding.txtDiemHs2.setText(diemHeSo2HocKy1);
            holder.binding.txtDiemHs3.setText(diemHeSo3HocKy1);
            TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
            trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();
            mauSo = diemHocKy1Model.getMauSo();
            diemHienTai = String.valueOf((double) Math.round(trungBinhKy1 * 10) / 10);
        } else if (hocKy.equals(Data.VALUE_KY_2)) {
            double trungBinhKy2 = 0;
            String diemHeSo1HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_1();
            String diemHeSo2HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_2();
            String diemHeSo3HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_3();
            holder.binding.txtDiemHs1.setText(diemHeSo1HocKy2);
            holder.binding.txtDiemHs2.setText(diemHeSo2HocKy2);
            holder.binding.txtDiemHs3.setText(diemHeSo3HocKy2);
            TinhDiemHocKyModel diemHocKy2Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
            trungBinhKy2 = diemHocKy2Model.getDiemTrungBinh();
            mauSo = diemHocKy2Model.getMauSo();
            diemHienTai = String.valueOf((double) Math.round(trungBinhKy2 * 10) / 10);
        } else {
            double trungBinhKy1 = 0, trungBinhKy2 = 0, trungBinhCaNam = 0;

            String diemHeSo1HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_1();
            String diemHeSo2HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_2();
            String diemHeSo3HocKy1 = list.get(position).getMonHoc().getHoc_ky_1().getDiem_he_so_3();

            TinhDiemHocKyModel diemHocKy1Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy1, diemHeSo2HocKy1, diemHeSo3HocKy1);
            int mauSoKy1 = diemHocKy1Model.getMauSo();
            trungBinhKy1 = diemHocKy1Model.getDiemTrungBinh();
            holder.binding.txtDiemHs1.setText(mauSoKy1 > 0 ? String.valueOf((double) Math.round(trungBinhKy1 * 10) / 10) : "");

            String diemHeSo1HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_1();
            String diemHeSo2HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_2();
            String diemHeSo3HocKy2 = list.get(position).getMonHoc().getHoc_ky_2().getDiem_he_so_3();
            TinhDiemHocKyModel diemHocKy2Model = Utils.getInstance().tinhDiemHocKy(diemHeSo1HocKy2, diemHeSo2HocKy2, diemHeSo3HocKy2);
            int mauSoKy2 = diemHocKy2Model.getMauSo();
            trungBinhKy2 = diemHocKy2Model.getDiemTrungBinh();
            holder.binding.txtDiemHs2.setText(mauSoKy2 > 0 ? String.valueOf((double) Math.round(trungBinhKy2 * 10) / 10) : "");
            holder.binding.txtDiemHs3.setText("");

            if (mauSoKy1 > 0 && mauSoKy2 > 0) {
                trungBinhCaNam = (trungBinhKy1 + 2 * trungBinhKy2) / 3;
            } else if (mauSoKy1 == 0) {
                trungBinhCaNam = trungBinhKy2;
            } else if (mauSoKy2 == 0) {
                trungBinhCaNam = trungBinhKy1;
            }
            mauSo = mauSoKy1 + mauSoKy2;
            diemHienTai = String.valueOf((double) Math.round(trungBinhCaNam * 10) / 10);
        }
        if (mauSo == 0) {
            holder.binding.txtTbm.setText("< -.- >");
        } else {
            holder.binding.txtTbm.setText(diemHienTai);
        }
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class BangDiemViewHolder extends RecyclerView.ViewHolder {
        private final ItemBangDiemBinding binding;

        public BangDiemViewHolder(ItemBangDiemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
