package com.leenguyen.studentnote.screens.monhocdetail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.databinding.ItemDiemBinding;

import java.util.List;

public class DiemAdapter extends RecyclerView.Adapter<DiemAdapter.DiemViewHolder> {
    private Context context;
    private List<String> list;
    private boolean isShowRemoveButton = false;
    private RemoveDiemListener listener;
    private int type;

    public DiemAdapter(Context context, int type, RemoveDiemListener listener) {
        this.context = context;
        this.listener = listener;
        this.type = type;
        isShowRemoveButton = false;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setShowRemoveButton(boolean isShowRemoveButton) {
        this.isShowRemoveButton = isShowRemoveButton;
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public boolean getShowRemoveButton() {
        return isShowRemoveButton;
    }

    @NonNull
    @Override
    public DiemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDiemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_diem, parent, false);
        return new DiemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DiemViewHolder holder, int position) {
        holder.binding.txtDiem.setText(list.get(position));
        holder.binding.btnRemove.setVisibility(isShowRemoveButton ? View.VISIBLE : View.GONE);
        holder.binding.btnRemove.setOnClickListener(v -> listener.removeDiemListener(type, position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class DiemViewHolder extends RecyclerView.ViewHolder {
        private final ItemDiemBinding binding;

        public DiemViewHolder(ItemDiemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
