package com.leenguyen.studentnote.screens.caidat.fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentDoiTenBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.screens.caidat.CaiDatActivity;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

public class DoiTenFragment extends BaseFragment<FragmentDoiTenBinding> {
    private String password = "", username, name;

    @Override
    protected void initLayout() {
        ((CaiDatActivity) getActivity()).setTitle(getString(R.string.doi_ten));

        name = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_NAME);
        username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        password = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_PASSWORD);
        binding.edtHoTen.setText(name);
        binding.edtHoTen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.txtError.setVisibility(s.length() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.btnUpdate.setOnClickListener(v -> updateHoTen());
    }

    private void updateHoTen() {
        if (!Utils.getInstance().isOnline(getContext())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.can_co_ket_noi_mang), null);
            return;
        }
        if (!password.equals(binding.edtMatKhau.getText().toString())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.mat_khau_khong_dung), null);
            return;
        }
        if (binding.edtHoTen.getText().length() == 0) {
            binding.txtError.setVisibility(View.VISIBLE);
            return;
        }
        if (name.equals(binding.edtHoTen.getText().toString())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.ten_khong_doi), null);
            return;
        }
        database.child(username).child(Data.ACCOUNT).child(Data.NAME).setValue(binding.edtHoTen.getText().toString().trim());
        DataUtils.getInstance().saveValue(getContext(), Data.DATA, Data.KEY_NAME, binding.edtHoTen.getText().toString().trim());
        binding.edtMatKhau.setText("");
        Toast.makeText(getContext(), getString(R.string.doi_ten_thanh_cong), Toast.LENGTH_SHORT).show();
        ((CaiDatActivity) getActivity()).openFragment(CaiDatActivity.MENU_POS);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_doi_ten;
    }
}
