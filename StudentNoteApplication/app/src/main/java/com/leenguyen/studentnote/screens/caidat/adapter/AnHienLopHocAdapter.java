package com.leenguyen.studentnote.screens.caidat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.ItemDanhSachLopBinding;
import com.leenguyen.studentnote.screens.caidat.model.AnHienLopHocModel;
import com.leenguyen.studentnote.utils.DataUtils;

import java.util.List;

public class AnHienLopHocAdapter extends RecyclerView.Adapter<AnHienLopHocAdapter.AnHienLopHocViewHolder> {
    private Context context;
    private List<AnHienLopHocModel> list;

    public AnHienLopHocAdapter(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<AnHienLopHocModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<AnHienLopHocModel> getData() {
        return list;
    }

    @NonNull
    @Override
    public AnHienLopHocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDanhSachLopBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_danh_sach_lop, parent, false);
        return new AnHienLopHocViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AnHienLopHocViewHolder holder, int position) {
        holder.binding.checkbox.setChecked(list.get(position).isDisplay());
        holder.binding.txtLop.setText(list.get(position).getTenLopHoc());
        holder.binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> list.get(position).setDisplay(isChecked));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class AnHienLopHocViewHolder extends RecyclerView.ViewHolder {
        private final ItemDanhSachLopBinding binding;

        public AnHienLopHocViewHolder(ItemDanhSachLopBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
