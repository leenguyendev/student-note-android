package com.leenguyen.studentnote.screens.themmonhoc.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.screens.main.adapter.ViewPagerAdapter;
import com.leenguyen.studentnote.screens.themmonhoc.ThemDanhSachMonHocFragment;
import com.leenguyen.studentnote.screens.themmonhoc.ThemMotMonHocFragment;

public class ThemMonHocViewPagerAdapter extends FragmentStateAdapter {


    public ThemMonHocViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1:
                return new ThemDanhSachMonHocFragment();
            default:
                return new ThemMotMonHocFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
