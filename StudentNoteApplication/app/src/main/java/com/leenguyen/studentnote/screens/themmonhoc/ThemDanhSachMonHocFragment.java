package com.leenguyen.studentnote.screens.themmonhoc;

import androidx.recyclerview.widget.GridLayoutManager;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseFragment;
import com.leenguyen.studentnote.constants.Data;
import com.leenguyen.studentnote.databinding.FragmentThemDanhSachMonHocBinding;
import com.leenguyen.studentnote.dialogs.NoticeDialog;
import com.leenguyen.studentnote.dialogs.OkButtonClickListener;
import com.leenguyen.studentnote.model.DiemMonHocModel;
import com.leenguyen.studentnote.model.MonHocModel;
import com.leenguyen.studentnote.screens.themmonhoc.adapter.ThemDanhSachMonHocAdapter;
import com.leenguyen.studentnote.utils.DataUtils;
import com.leenguyen.studentnote.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ThemDanhSachMonHocFragment extends BaseFragment<FragmentThemDanhSachMonHocBinding> implements SaveClickListener, OkButtonClickListener {
    private List<String> list;
    private ThemDanhSachMonHocAdapter adapter;

    @Override
    protected void initLayout() {
        ((ThemMonHocActivity) getActivity()).setSaveListener(this);
        adapter = new ThemDanhSachMonHocAdapter(getContext());
        GridLayoutManager manager = new GridLayoutManager(getContext(), 1);
        binding.rcvMonHoc.setLayoutManager(manager);
        binding.rcvMonHoc.setAdapter(adapter);
        String[] danhSachMonHoc;
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        if (Integer.parseInt(lop) >= 10) {
            danhSachMonHoc = getResources().getStringArray(R.array.array_mon_hoc_cap_3);
        } else if (Integer.parseInt(lop) >= 6) {
            danhSachMonHoc = getResources().getStringArray(R.array.array_mon_hoc_cap_2);
        } else {
            danhSachMonHoc = getResources().getStringArray(R.array.array_mon_hoc_cap_1);
        }
        list = new ArrayList<>();
        list.addAll(Arrays.asList(danhSachMonHoc));
        adapter.setData(list);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_them_danh_sach_mon_hoc;
    }

    @Override
    public void saveMotMonListener() {
        //Not implement
    }

    @Override
    public void saveDanhSachListener() {
        NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.chu_y), getString(R.string.chu_y_khi_them_danh_sach_mon_hoc), this);
    }

    @Override
    public void okButtonClickListener() {
        if (!Utils.getInstance().isOnline(getContext())) {
            NoticeDialog.getInstance().showNoticeDialog(getContext(), getString(R.string.error), getString(R.string.can_co_ket_noi_mang), null);
            return;
        }
        List<String> listAdded = adapter.getListAdded();
        String username = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_USERNAME);
        String lop = DataUtils.getInstance().getValue(getContext(), Data.DATA, Data.KEY_CLASS);
        for (int i = 0; i < listAdded.size(); i++) {
            String monHoc = listAdded.get(i);
            if (monHoc != null) {
                DiemMonHocModel hocKy1 = new DiemMonHocModel("", "", "", "100");
                DiemMonHocModel hocKy2 = new DiemMonHocModel("", "", "", "100");
                MonHocModel monHocModel = new MonHocModel(monHoc, "100", "1", "0", hocKy1, hocKy2);
                database.child(username).child(lop).child(Data.MON_HOC).child(String.valueOf(System.currentTimeMillis() / 1000 + i)).setValue(monHocModel);
            }
        }
        getActivity().finish();
    }
}
