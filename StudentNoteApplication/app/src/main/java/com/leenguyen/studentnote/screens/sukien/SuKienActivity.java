package com.leenguyen.studentnote.screens.sukien;

import androidx.fragment.app.Fragment;

import android.view.View;

import com.leenguyen.studentnote.R;
import com.leenguyen.studentnote.bases.BaseActivity;
import com.leenguyen.studentnote.databinding.ActivitySuKienBinding;
import com.leenguyen.studentnote.screens.sukien.fragment.SuKienFragmnet;
import com.leenguyen.studentnote.screens.sukien.fragment.ThemSuKienFragment;

public class SuKienActivity extends BaseActivity<ActivitySuKienBinding> {
    private Fragment[] fragments;
    public static final int SU_KIEN_LIST_POS = 0;
    public static final int THEM_SU_KIEN_POS = 1;
    private int currentPos = 0;
    private SaveClickListener listener;


    @Override
    protected void initLayout() {
        fragments = new Fragment[]{new SuKienFragmnet(), new ThemSuKienFragment()};
        binding.btnBack.setOnClickListener(v -> {
            if (currentPos == SU_KIEN_LIST_POS) {
                finish();
            } else {
                openFragment(SU_KIEN_LIST_POS);
            }
        });
        binding.btnAdd.setOnClickListener(v -> openFragment(THEM_SU_KIEN_POS));
        binding.btnSave.setOnClickListener(v -> listener.saveClickListener());
        openFragment(SU_KIEN_LIST_POS);
    }

    public void openFragment(int pos) {
        binding.btnAdd.setVisibility(pos == SU_KIEN_LIST_POS ? View.VISIBLE : View.GONE);
        binding.btnSave.setVisibility(pos == THEM_SU_KIEN_POS ? View.VISIBLE : View.GONE);
        currentPos = pos;
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragments[pos], "openFragment")
                .addToBackStack(null)
                .commit();
    }

    public void setTitle(String title) {
        binding.txtTitle.setText(title);
    }

    public void setSaveClickListener(SaveClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBackPressed() {
        if (currentPos == SU_KIEN_LIST_POS) {
            finish();
        } else {
            openFragment(SU_KIEN_LIST_POS);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_su_kien;
    }

}